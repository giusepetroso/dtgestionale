﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ControlMainMenu
    Inherits System.Windows.Forms.UserControl

    'UserControl esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla mediante l'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.BtnPageClienti = New System.Windows.Forms.Button()
        Me.BtnPageCommesse = New System.Windows.Forms.Button()
        Me.BtnPageHome = New System.Windows.Forms.Button()
        Me.BtnPageFornitori = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'BtnPageClienti
        '
        Me.BtnPageClienti.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnPageClienti.BackColor = System.Drawing.Color.Snow
        Me.BtnPageClienti.FlatAppearance.BorderSize = 0
        Me.BtnPageClienti.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnPageClienti.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnPageClienti.Location = New System.Drawing.Point(8, 104)
        Me.BtnPageClienti.Name = "BtnPageClienti"
        Me.BtnPageClienti.Size = New System.Drawing.Size(146, 40)
        Me.BtnPageClienti.TabIndex = 0
        Me.BtnPageClienti.Text = "CLIENTI"
        Me.BtnPageClienti.UseVisualStyleBackColor = False
        '
        'BtnPageCommesse
        '
        Me.BtnPageCommesse.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnPageCommesse.BackColor = System.Drawing.Color.Snow
        Me.BtnPageCommesse.FlatAppearance.BorderSize = 0
        Me.BtnPageCommesse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnPageCommesse.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnPageCommesse.Location = New System.Drawing.Point(8, 56)
        Me.BtnPageCommesse.Name = "BtnPageCommesse"
        Me.BtnPageCommesse.Size = New System.Drawing.Size(146, 40)
        Me.BtnPageCommesse.TabIndex = 1
        Me.BtnPageCommesse.Text = "COMMESSE"
        Me.BtnPageCommesse.UseVisualStyleBackColor = False
        '
        'BtnPageHome
        '
        Me.BtnPageHome.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnPageHome.BackColor = System.Drawing.Color.Snow
        Me.BtnPageHome.FlatAppearance.BorderSize = 0
        Me.BtnPageHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnPageHome.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnPageHome.Location = New System.Drawing.Point(8, 8)
        Me.BtnPageHome.Name = "BtnPageHome"
        Me.BtnPageHome.Size = New System.Drawing.Size(146, 40)
        Me.BtnPageHome.TabIndex = 2
        Me.BtnPageHome.Text = "PARTICOLARI"
        Me.BtnPageHome.UseVisualStyleBackColor = False
        '
        'BtnPageFornitori
        '
        Me.BtnPageFornitori.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnPageFornitori.BackColor = System.Drawing.Color.Snow
        Me.BtnPageFornitori.FlatAppearance.BorderSize = 0
        Me.BtnPageFornitori.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnPageFornitori.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnPageFornitori.Location = New System.Drawing.Point(8, 152)
        Me.BtnPageFornitori.Name = "BtnPageFornitori"
        Me.BtnPageFornitori.Size = New System.Drawing.Size(146, 40)
        Me.BtnPageFornitori.TabIndex = 3
        Me.BtnPageFornitori.Text = "FORNITORI"
        Me.BtnPageFornitori.UseVisualStyleBackColor = False
        '
        'ControlMainMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DodgerBlue
        Me.Controls.Add(Me.BtnPageFornitori)
        Me.Controls.Add(Me.BtnPageHome)
        Me.Controls.Add(Me.BtnPageCommesse)
        Me.Controls.Add(Me.BtnPageClienti)
        Me.Name = "ControlMainMenu"
        Me.Size = New System.Drawing.Size(160, 512)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents BtnPageClienti As Button
    Friend WithEvents BtnPageCommesse As Button
    Friend WithEvents BtnPageHome As Button
    Friend WithEvents BtnPageFornitori As Button
End Class
