﻿Public Class ControlMainMenu
    Property Opened = False

    Public Sub Init()
        FormMain.Controls.Add(Me)
        FormMain.Controls.SetChildIndex(Me, 0)
        Me.Height = FormMain.Height
        Me.Anchor = AnchorStyles.Top Or AnchorStyles.Bottom
        Me.Show()
        Me.Dock = DockStyle.Left
        Me.Visible = False
    End Sub

    Public Sub ShowMenu()
        Me.Visible = True
        Me.Dock = DockStyle.Left
        Opened = True
    End Sub

    Public Sub HideMenu()
        Me.Visible = False
        Me.Dock = DockStyle.None
        Opened = False
    End Sub

    Public Sub ToggleMenu()
        If Me.Opened Then
            Me.HideMenu()
        Else
            Me.ShowMenu()
        End If
    End Sub

    Private Sub BtnPageHome_Click(sender As Object, e As EventArgs) Handles BtnPageHome.Click
        PageManager.OpenPage("home")
        Me.HideMenu()
    End Sub

    Private Sub BtnPageCommesse_Click(sender As Object, e As EventArgs) Handles BtnPageCommesse.Click
        PageManager.OpenPage("commesse")
        Me.HideMenu()
    End Sub

    Private Sub BtnPageClienti_Click(sender As Object, e As EventArgs) Handles BtnPageClienti.Click
        PageManager.OpenPage("clienti")
        Me.HideMenu()
    End Sub

    Private Sub BtnPageFornitori_Click(sender As Object, e As EventArgs) Handles BtnPageFornitori.Click
        PageManager.OpenPage("fornitori")
        Me.HideMenu()
    End Sub
End Class
