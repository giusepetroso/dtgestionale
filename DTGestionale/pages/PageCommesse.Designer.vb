﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class PageCommesse
    Inherits System.Windows.Forms.UserControl

    'UserControl esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla mediante l'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.PnlMainContainer = New System.Windows.Forms.Panel()
        Me.BtnAnagCommesseAdd = New System.Windows.Forms.Button()
        Me.DgvAnagCommesse = New System.Windows.Forms.DataGridView()
        Me.id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.id_cliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.codice = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ragione_cliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.azioniEdit = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.azioniDelete = New System.Windows.Forms.DataGridViewButtonColumn()
        CType(Me.DgvAnagCommesse, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PnlMainContainer
        '
        Me.PnlMainContainer.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PnlMainContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PnlMainContainer.Location = New System.Drawing.Point(0, 0)
        Me.PnlMainContainer.Name = "PnlMainContainer"
        Me.PnlMainContainer.Size = New System.Drawing.Size(1024, 768)
        Me.PnlMainContainer.TabIndex = 0
        '
        'BtnAnagCommesseAdd
        '
        Me.BtnAnagCommesseAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnAnagCommesseAdd.BackColor = System.Drawing.Color.DodgerBlue
        Me.BtnAnagCommesseAdd.FlatAppearance.BorderSize = 0
        Me.BtnAnagCommesseAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnAnagCommesseAdd.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnAnagCommesseAdd.ForeColor = System.Drawing.Color.Snow
        Me.BtnAnagCommesseAdd.Location = New System.Drawing.Point(856, 4)
        Me.BtnAnagCommesseAdd.Name = "BtnAnagCommesseAdd"
        Me.BtnAnagCommesseAdd.Size = New System.Drawing.Size(162, 48)
        Me.BtnAnagCommesseAdd.TabIndex = 5
        Me.BtnAnagCommesseAdd.Text = "+ NUOVO"
        Me.BtnAnagCommesseAdd.UseVisualStyleBackColor = False
        '
        'DgvAnagCommesse
        '
        Me.DgvAnagCommesse.AllowUserToAddRows = False
        Me.DgvAnagCommesse.AllowUserToDeleteRows = False
        Me.DgvAnagCommesse.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DgvAnagCommesse.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DgvAnagCommesse.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.id, Me.id_cliente, Me.codice, Me.ragione_cliente, Me.azioniEdit, Me.azioniDelete})
        Me.DgvAnagCommesse.Location = New System.Drawing.Point(0, 60)
        Me.DgvAnagCommesse.Name = "DgvAnagCommesse"
        Me.DgvAnagCommesse.ReadOnly = True
        Me.DgvAnagCommesse.Size = New System.Drawing.Size(1024, 704)
        Me.DgvAnagCommesse.TabIndex = 4
        '
        'id
        '
        Me.id.DataPropertyName = "id"
        Me.id.HeaderText = "id"
        Me.id.Name = "id"
        Me.id.ReadOnly = True
        Me.id.Visible = False
        '
        'id_cliente
        '
        Me.id_cliente.HeaderText = "id_cliente"
        Me.id_cliente.Name = "id_cliente"
        Me.id_cliente.ReadOnly = True
        Me.id_cliente.Visible = False
        '
        'codice
        '
        Me.codice.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.codice.DataPropertyName = "codice"
        Me.codice.FillWeight = 30.0!
        Me.codice.HeaderText = "Codice Commessa"
        Me.codice.Name = "codice"
        Me.codice.ReadOnly = True
        '
        'ragione_cliente
        '
        Me.ragione_cliente.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ragione_cliente.DataPropertyName = "ragione_cliente"
        Me.ragione_cliente.FillWeight = 30.0!
        Me.ragione_cliente.HeaderText = "Cliente"
        Me.ragione_cliente.Name = "ragione_cliente"
        Me.ragione_cliente.ReadOnly = True
        '
        'azioniEdit
        '
        Me.azioniEdit.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.azioniEdit.HeaderText = "Modifica"
        Me.azioniEdit.Name = "azioniEdit"
        Me.azioniEdit.ReadOnly = True
        Me.azioniEdit.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.azioniEdit.Text = "M"
        Me.azioniEdit.UseColumnTextForButtonValue = True
        Me.azioniEdit.Width = 60
        '
        'azioniDelete
        '
        Me.azioniDelete.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.azioniDelete.HeaderText = "Elimina"
        Me.azioniDelete.Name = "azioniDelete"
        Me.azioniDelete.ReadOnly = True
        Me.azioniDelete.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.azioniDelete.Text = "E"
        Me.azioniDelete.UseColumnTextForButtonValue = True
        Me.azioniDelete.Width = 60
        '
        'PageCommesse
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Snow
        Me.Controls.Add(Me.BtnAnagCommesseAdd)
        Me.Controls.Add(Me.DgvAnagCommesse)
        Me.Controls.Add(Me.PnlMainContainer)
        Me.Name = "PageCommesse"
        Me.Size = New System.Drawing.Size(1024, 768)
        CType(Me.DgvAnagCommesse, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents PnlMainContainer As Panel
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents BtnAnagCommesseAdd As Button
    Friend WithEvents DgvAnagCommesse As DataGridView
    Friend WithEvents id As DataGridViewTextBoxColumn
    Friend WithEvents id_cliente As DataGridViewTextBoxColumn
    Friend WithEvents codice As DataGridViewTextBoxColumn
    Friend WithEvents ragione_cliente As DataGridViewTextBoxColumn
    Friend WithEvents azioniEdit As DataGridViewButtonColumn
    Friend WithEvents azioniDelete As DataGridViewButtonColumn
End Class
