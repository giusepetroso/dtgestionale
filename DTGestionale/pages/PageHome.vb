﻿Imports DTGestionale

Public Class PageHome
    Implements ITablePage, IPage

    Private pageLoaded = False

    '#######################################################
#Region "EVENTI"
    'LOAD
    Private Sub PageHome_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        pageLoaded = True
    End Sub

    'PULSANTI IN TABELLA
    Private Sub DgvHome_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DgvHome.CellContentClick
        Dim senderGrid = DirectCast(sender, DataGridView)

        If TypeOf senderGrid.Columns(e.ColumnIndex) Is DataGridViewButtonColumn AndAlso e.RowIndex >= 0 Then
            Dim row As DataGridViewRow = senderGrid.Rows(e.RowIndex)
            Dim cell As DataGridViewColumn = senderGrid.Columns(e.ColumnIndex)
            Dim id As Integer = row.Cells("id").Value
            Dim codice As String = row.Cells("codice").Value
            Select Case cell.Name
                Case "azioniEdit" 'EDIT
                    'TODO
                Case "azioniDelete" 'DELETE
                    Dim deleteMsg = "L'eliminazione di un particolare comporta la cancellazione di tutti i dati ad esso associati (es. richieste di preventivo) " + vbNewLine + vbNewLine + "Eliminare comunque il particolare con codice " + codice + "?"
                    Dim deleteMsgBox = MessageBox.Show(deleteMsg, "Eliminazione Particolare", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                    If deleteMsgBox = DialogResult.Yes Then
                        DeleteRow("particolari", id)
                        Me.DrawTable()
                    End If
                Case Else 'VIEW
                    Dim params As New Dictionary(Of String, Object)
                    params.Add("id", id)
                    PageManager.OpenPage("particolare", params)
            End Select
        End If
    End Sub

    'PULSANTE CERCA
    Private Sub BtnParticolariCerca_Click(sender As Object, e As EventArgs) Handles BtnParticolariCerca.Click
        FormParticolariFind.Show()
    End Sub

    'PULSANTE IMPORTA
    Private Sub BtnParticolariImport_Click(sender As Object, e As EventArgs) Handles BtnParticolariImport.Click
        FormDistintaImport.Show()
    End Sub

    'CHECKBOX FILTRO SOLO COSTRUTTIVI
    Private Sub ChkOnly101_CheckedChanged(sender As Object, e As EventArgs) Handles ChkOnly101.CheckedChanged
        If pageLoaded Then Me.DrawTable()
    End Sub
#End Region

    '#######################################################
#Region "FUNZIONI"
    Private Sub OnOpen() Implements IPage.OnOpen
        Me.DrawTable()
    End Sub

    Public Sub DrawTable() Implements ITablePage.DrawTable
        Dim dbRecord As New DbModel("particolari_dettaglio")

        Dim res As List(Of Dictionary(Of String, Object))
        res = dbRecord.GetAll("dataora_modifica DESC, id_categoria ASC")

        DgvHome.Rows.Clear()
        DgvHome.Refresh()

        For Each r As Dictionary(Of String, Object) In res

            'CHECK FILTERS
            If ChkOnly101.Checked And r("id_categoria") <> 101 Then Continue For

            'ADD ROWS
            DgvHome.Rows.Add()
            For Each c As KeyValuePair(Of String, Object) In r
                If DgvHome.Columns.Contains(c.Key) Then
                    DgvHome.Rows(DgvHome.Rows.Count - 1).Cells(c.Key).Value = c.Value
                End If
            Next
        Next
    End Sub

    Private Function DeleteRow(table, id) As Boolean
        Dim dbRecord As New DbModel(table)
        Dim cnd As New Dictionary(Of String, Object)
        cnd.Add("id", id)
        Return dbRecord.Delete(cnd)
    End Function
#End Region

End Class
