﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class PageBase
    Inherits System.Windows.Forms.UserControl

    'UserControl esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla mediante l'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.PnlMainContainer = New System.Windows.Forms.Panel()
        Me.SuspendLayout()
        '
        'PnlMainContainer
        '
        Me.PnlMainContainer.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PnlMainContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PnlMainContainer.Location = New System.Drawing.Point(0, 0)
        Me.PnlMainContainer.Name = "PnlMainContainer"
        Me.PnlMainContainer.Size = New System.Drawing.Size(1024, 768)
        Me.PnlMainContainer.TabIndex = 0
        '
        'PageBase
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Snow
        Me.Controls.Add(Me.PnlMainContainer)
        Me.Name = "PageBase"
        Me.Size = New System.Drawing.Size(1024, 768)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents PnlMainContainer As Panel
    Friend WithEvents DataGridView1 As DataGridView
End Class
