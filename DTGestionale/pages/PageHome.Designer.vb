﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class PageHome
    Inherits System.Windows.Forms.UserControl

    'UserControl esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla mediante l'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.PnlMainContainer = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ChkOnly101 = New System.Windows.Forms.CheckBox()
        Me.BtnParticolariImport = New System.Windows.Forms.Button()
        Me.DgvHome = New System.Windows.Forms.DataGridView()
        Me.id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.id_cliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.id_commessa = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.id_categoria = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.id_stato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.id_tipo_lavorazione = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.id_tipo_trattamento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dataora_creazione = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ragione_cliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.codice_commessa = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nome_categoria = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nome_stato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.codice = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.descrizione = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nome_tipo_lavorazione = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nome_tipo_trattamento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.quantita = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dataora_modifica = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.azioniView = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.azioniEdit = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.azioniDelete = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.BtnParticolariCerca = New System.Windows.Forms.Button()
        Me.PnlMainContainer.SuspendLayout()
        CType(Me.DgvHome, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PnlMainContainer
        '
        Me.PnlMainContainer.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PnlMainContainer.Controls.Add(Me.BtnParticolariCerca)
        Me.PnlMainContainer.Controls.Add(Me.Label1)
        Me.PnlMainContainer.Controls.Add(Me.ChkOnly101)
        Me.PnlMainContainer.Controls.Add(Me.BtnParticolariImport)
        Me.PnlMainContainer.Controls.Add(Me.DgvHome)
        Me.PnlMainContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PnlMainContainer.Location = New System.Drawing.Point(0, 0)
        Me.PnlMainContainer.Name = "PnlMainContainer"
        Me.PnlMainContainer.Size = New System.Drawing.Size(1024, 768)
        Me.PnlMainContainer.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label1.Location = New System.Drawing.Point(8, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 16)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Filtro"
        '
        'ChkOnly101
        '
        Me.ChkOnly101.AutoSize = True
        Me.ChkOnly101.Checked = True
        Me.ChkOnly101.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkOnly101.Location = New System.Drawing.Point(8, 32)
        Me.ChkOnly101.Name = "ChkOnly101"
        Me.ChkOnly101.Size = New System.Drawing.Size(95, 17)
        Me.ChkOnly101.TabIndex = 9
        Me.ChkOnly101.Text = "Solo costruttivi"
        Me.ChkOnly101.UseVisualStyleBackColor = True
        '
        'BtnParticolariImport
        '
        Me.BtnParticolariImport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnParticolariImport.BackColor = System.Drawing.Color.DodgerBlue
        Me.BtnParticolariImport.FlatAppearance.BorderSize = 0
        Me.BtnParticolariImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnParticolariImport.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnParticolariImport.ForeColor = System.Drawing.Color.Snow
        Me.BtnParticolariImport.Location = New System.Drawing.Point(856, 8)
        Me.BtnParticolariImport.Name = "BtnParticolariImport"
        Me.BtnParticolariImport.Size = New System.Drawing.Size(162, 48)
        Me.BtnParticolariImport.TabIndex = 7
        Me.BtnParticolariImport.Text = "+ IMPORTA"
        Me.BtnParticolariImport.UseVisualStyleBackColor = False
        '
        'DgvHome
        '
        Me.DgvHome.AllowUserToAddRows = False
        Me.DgvHome.AllowUserToDeleteRows = False
        Me.DgvHome.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DgvHome.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DgvHome.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.id, Me.id_cliente, Me.id_commessa, Me.id_categoria, Me.id_stato, Me.id_tipo_lavorazione, Me.id_tipo_trattamento, Me.dataora_creazione, Me.ragione_cliente, Me.codice_commessa, Me.nome_categoria, Me.nome_stato, Me.codice, Me.descrizione, Me.nome_tipo_lavorazione, Me.nome_tipo_trattamento, Me.quantita, Me.dataora_modifica, Me.azioniView, Me.azioniEdit, Me.azioniDelete})
        Me.DgvHome.Location = New System.Drawing.Point(0, 64)
        Me.DgvHome.Name = "DgvHome"
        Me.DgvHome.ReadOnly = True
        Me.DgvHome.Size = New System.Drawing.Size(1024, 704)
        Me.DgvHome.TabIndex = 4
        '
        'id
        '
        Me.id.DataPropertyName = "id"
        Me.id.HeaderText = "id"
        Me.id.Name = "id"
        Me.id.ReadOnly = True
        Me.id.Visible = False
        '
        'id_cliente
        '
        Me.id_cliente.DataPropertyName = "id_cliente"
        Me.id_cliente.HeaderText = "id_cliente"
        Me.id_cliente.Name = "id_cliente"
        Me.id_cliente.ReadOnly = True
        Me.id_cliente.Visible = False
        '
        'id_commessa
        '
        Me.id_commessa.DataPropertyName = "id_commessa"
        Me.id_commessa.HeaderText = "id_commessa"
        Me.id_commessa.Name = "id_commessa"
        Me.id_commessa.ReadOnly = True
        Me.id_commessa.Visible = False
        '
        'id_categoria
        '
        Me.id_categoria.DataPropertyName = "id_categoria"
        Me.id_categoria.HeaderText = "id_categoria"
        Me.id_categoria.Name = "id_categoria"
        Me.id_categoria.ReadOnly = True
        Me.id_categoria.Visible = False
        '
        'id_stato
        '
        Me.id_stato.DataPropertyName = "id_stato"
        Me.id_stato.HeaderText = "id_stato"
        Me.id_stato.Name = "id_stato"
        Me.id_stato.ReadOnly = True
        Me.id_stato.Visible = False
        '
        'id_tipo_lavorazione
        '
        Me.id_tipo_lavorazione.HeaderText = "id_tipo_lavorazione"
        Me.id_tipo_lavorazione.Name = "id_tipo_lavorazione"
        Me.id_tipo_lavorazione.ReadOnly = True
        Me.id_tipo_lavorazione.Visible = False
        '
        'id_tipo_trattamento
        '
        Me.id_tipo_trattamento.HeaderText = "id_tipo_trattamento"
        Me.id_tipo_trattamento.Name = "id_tipo_trattamento"
        Me.id_tipo_trattamento.ReadOnly = True
        Me.id_tipo_trattamento.Visible = False
        '
        'dataora_creazione
        '
        Me.dataora_creazione.DataPropertyName = "dataora_creazione"
        Me.dataora_creazione.HeaderText = "dataora_creazione"
        Me.dataora_creazione.Name = "dataora_creazione"
        Me.dataora_creazione.ReadOnly = True
        Me.dataora_creazione.Visible = False
        '
        'ragione_cliente
        '
        Me.ragione_cliente.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ragione_cliente.DataPropertyName = "ragione_cliente"
        Me.ragione_cliente.FillWeight = 10.0!
        Me.ragione_cliente.HeaderText = "Cliente"
        Me.ragione_cliente.Name = "ragione_cliente"
        Me.ragione_cliente.ReadOnly = True
        '
        'codice_commessa
        '
        Me.codice_commessa.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.codice_commessa.DataPropertyName = "codice_commessa"
        Me.codice_commessa.FillWeight = 15.0!
        Me.codice_commessa.HeaderText = "Commessa"
        Me.codice_commessa.Name = "codice_commessa"
        Me.codice_commessa.ReadOnly = True
        '
        'nome_categoria
        '
        Me.nome_categoria.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.nome_categoria.DataPropertyName = "nome_categoria"
        Me.nome_categoria.FillWeight = 20.0!
        Me.nome_categoria.HeaderText = "Categoria"
        Me.nome_categoria.Name = "nome_categoria"
        Me.nome_categoria.ReadOnly = True
        '
        'nome_stato
        '
        Me.nome_stato.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.nome_stato.DataPropertyName = "nome_stato"
        Me.nome_stato.FillWeight = 10.0!
        Me.nome_stato.HeaderText = "Stato"
        Me.nome_stato.Name = "nome_stato"
        Me.nome_stato.ReadOnly = True
        '
        'codice
        '
        Me.codice.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.codice.DataPropertyName = "codice"
        Me.codice.FillWeight = 25.0!
        Me.codice.HeaderText = "Codice particolare"
        Me.codice.Name = "codice"
        Me.codice.ReadOnly = True
        '
        'descrizione
        '
        Me.descrizione.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.descrizione.DataPropertyName = "descrizione"
        Me.descrizione.FillWeight = 40.0!
        Me.descrizione.HeaderText = "Descrizione"
        Me.descrizione.Name = "descrizione"
        Me.descrizione.ReadOnly = True
        '
        'nome_tipo_lavorazione
        '
        Me.nome_tipo_lavorazione.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.nome_tipo_lavorazione.DataPropertyName = "nome_tipo_lavorazione"
        Me.nome_tipo_lavorazione.FillWeight = 15.0!
        Me.nome_tipo_lavorazione.HeaderText = "Lavorazione"
        Me.nome_tipo_lavorazione.Name = "nome_tipo_lavorazione"
        Me.nome_tipo_lavorazione.ReadOnly = True
        '
        'nome_tipo_trattamento
        '
        Me.nome_tipo_trattamento.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.nome_tipo_trattamento.DataPropertyName = "nome_tipo_trattamento"
        Me.nome_tipo_trattamento.FillWeight = 15.0!
        Me.nome_tipo_trattamento.HeaderText = "Trattamento"
        Me.nome_tipo_trattamento.Name = "nome_tipo_trattamento"
        Me.nome_tipo_trattamento.ReadOnly = True
        '
        'quantita
        '
        Me.quantita.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.quantita.DataPropertyName = "quantita"
        Me.quantita.FillWeight = 5.0!
        Me.quantita.HeaderText = "Qta"
        Me.quantita.Name = "quantita"
        Me.quantita.ReadOnly = True
        '
        'dataora_modifica
        '
        Me.dataora_modifica.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dataora_modifica.DataPropertyName = "dataora_modifica"
        Me.dataora_modifica.FillWeight = 20.0!
        Me.dataora_modifica.HeaderText = "Ultima modifica"
        Me.dataora_modifica.Name = "dataora_modifica"
        Me.dataora_modifica.ReadOnly = True
        '
        'azioniView
        '
        Me.azioniView.HeaderText = "Dettaglio"
        Me.azioniView.Name = "azioniView"
        Me.azioniView.ReadOnly = True
        Me.azioniView.Text = "D"
        Me.azioniView.UseColumnTextForButtonValue = True
        Me.azioniView.Width = 55
        '
        'azioniEdit
        '
        Me.azioniEdit.HeaderText = "Modifica"
        Me.azioniEdit.Name = "azioniEdit"
        Me.azioniEdit.ReadOnly = True
        Me.azioniEdit.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.azioniEdit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.azioniEdit.Text = "M"
        Me.azioniEdit.UseColumnTextForButtonValue = True
        Me.azioniEdit.Width = 55
        '
        'azioniDelete
        '
        Me.azioniDelete.HeaderText = "Elimina"
        Me.azioniDelete.Name = "azioniDelete"
        Me.azioniDelete.ReadOnly = True
        Me.azioniDelete.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.azioniDelete.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.azioniDelete.Text = "E"
        Me.azioniDelete.UseColumnTextForButtonValue = True
        Me.azioniDelete.Width = 55
        '
        'BtnParticolariCerca
        '
        Me.BtnParticolariCerca.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnParticolariCerca.BackColor = System.Drawing.Color.DodgerBlue
        Me.BtnParticolariCerca.FlatAppearance.BorderSize = 0
        Me.BtnParticolariCerca.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnParticolariCerca.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnParticolariCerca.ForeColor = System.Drawing.Color.Snow
        Me.BtnParticolariCerca.Location = New System.Drawing.Point(680, 8)
        Me.BtnParticolariCerca.Name = "BtnParticolariCerca"
        Me.BtnParticolariCerca.Size = New System.Drawing.Size(162, 48)
        Me.BtnParticolariCerca.TabIndex = 11
        Me.BtnParticolariCerca.Text = "CERCA"
        Me.BtnParticolariCerca.UseVisualStyleBackColor = False
        '
        'PageHome
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Snow
        Me.Controls.Add(Me.PnlMainContainer)
        Me.Name = "PageHome"
        Me.Size = New System.Drawing.Size(1024, 768)
        Me.PnlMainContainer.ResumeLayout(False)
        Me.PnlMainContainer.PerformLayout()
        CType(Me.DgvHome, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents PnlMainContainer As Panel
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents BtnParticolariImport As Button
    Friend WithEvents DgvHome As DataGridView
    Friend WithEvents ChkOnly101 As CheckBox
    Friend WithEvents Label1 As Label
    Friend WithEvents id As DataGridViewTextBoxColumn
    Friend WithEvents id_cliente As DataGridViewTextBoxColumn
    Friend WithEvents id_commessa As DataGridViewTextBoxColumn
    Friend WithEvents id_categoria As DataGridViewTextBoxColumn
    Friend WithEvents id_stato As DataGridViewTextBoxColumn
    Friend WithEvents id_tipo_lavorazione As DataGridViewTextBoxColumn
    Friend WithEvents id_tipo_trattamento As DataGridViewTextBoxColumn
    Friend WithEvents dataora_creazione As DataGridViewTextBoxColumn
    Friend WithEvents ragione_cliente As DataGridViewTextBoxColumn
    Friend WithEvents codice_commessa As DataGridViewTextBoxColumn
    Friend WithEvents nome_categoria As DataGridViewTextBoxColumn
    Friend WithEvents nome_stato As DataGridViewTextBoxColumn
    Friend WithEvents codice As DataGridViewTextBoxColumn
    Friend WithEvents descrizione As DataGridViewTextBoxColumn
    Friend WithEvents nome_tipo_lavorazione As DataGridViewTextBoxColumn
    Friend WithEvents nome_tipo_trattamento As DataGridViewTextBoxColumn
    Friend WithEvents quantita As DataGridViewTextBoxColumn
    Friend WithEvents dataora_modifica As DataGridViewTextBoxColumn
    Friend WithEvents azioniView As DataGridViewButtonColumn
    Friend WithEvents azioniEdit As DataGridViewButtonColumn
    Friend WithEvents azioniDelete As DataGridViewButtonColumn
    Friend WithEvents BtnParticolariCerca As Button
End Class
