﻿Imports DTGestionale

Public Class PageFornitori
    Implements ITablePage, IPage

    Private pageLoaded = False

    '#######################################################
#Region "EVENTI"
    'LOAD
    Private Sub PageFornitori_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        pageLoaded = True
    End Sub

    'PULSANTI IN TABELLA
    Private Sub DgvAnagFornitori_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DgvAnagFornitori.CellContentClick
        Dim senderGrid = DirectCast(sender, DataGridView)

        If TypeOf senderGrid.Columns(e.ColumnIndex) Is DataGridViewButtonColumn AndAlso e.RowIndex >= 0 Then
            Dim row As DataGridViewRow = senderGrid.Rows(e.RowIndex)
            Dim cell As DataGridViewColumn = senderGrid.Columns(e.ColumnIndex)
            Dim id As Integer = row.Cells("id").Value
            Dim ragione As String = row.Cells("ragione").Value
            Select Case cell.Name
                Case "azioniEdit" 'EDIT
                    Console.WriteLine("EDIT")
                Case "azioniDelete" 'DELETE
                    Dim deleteMsg = "L'eliminazione di un fornitore comporta la cancellazione di tutti i dati ad esso associati (es. richieste di preventivo) " + vbNewLine + vbNewLine + "Eliminare comunque il fornitore " + ragione + "?"
                    Dim deleteMsgBox = MessageBox.Show(deleteMsg, "Eliminazione Fornitore", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                    If deleteMsgBox = DialogResult.Yes Then
                        DeleteRow("fornitori", id)
                        Me.DrawTable()
                    End If
                Case Else 'VIEW
                    Console.WriteLine("VIEW")
            End Select
        End If
    End Sub

    'PULSANTE AGGIUNGI CLIENTE
    Private Sub BtnAnagFornitoriAdd_Click(sender As Object, e As EventArgs) Handles BtnAnagFornitoriAdd.Click
        FormFornitoriAdd.Show()
    End Sub
#End Region

    '#######################################################
#Region "FUNZIONI"
    Private Sub OnOpen() Implements IPage.OnOpen
        Me.DrawTable()
    End Sub

    Public Sub DrawTable() Implements ITablePage.DrawTable
        Dim dbRecord As New DbModel("fornitori")

        Dim res As List(Of Dictionary(Of String, Object))
        res = dbRecord.GetAll("ragione ASC")

        DgvAnagFornitori.Rows.Clear()
        DgvAnagFornitori.Refresh()

        For Each r As Dictionary(Of String, Object) In res

            'CHECK FILTERS
            'filtri vari

            'ADD ROWS
            DgvAnagFornitori.Rows.Add()
            For Each c As KeyValuePair(Of String, Object) In r
                If DgvAnagFornitori.Columns.Contains(c.Key) Then
                    DgvAnagFornitori.Rows(DgvAnagFornitori.Rows.Count - 1).Cells(c.Key).Value = c.Value
                End If
            Next
        Next
    End Sub

    Private Function DeleteRow(table, id) As Boolean
        Dim dbRecord As New DbModel(table)
        Dim cnd As New Dictionary(Of String, Object)
        cnd.Add("id", id)
        Return dbRecord.Delete(cnd)
    End Function
#End Region

End Class
