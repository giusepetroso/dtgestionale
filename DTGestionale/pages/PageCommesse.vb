﻿Imports DTGestionale

Public Class PageCommesse
    Implements ITablePage, IPage

    Private pageLoaded = False

    '#######################################################
#Region "EVENTI"
    'LOAD
    Private Sub PageHome_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        pageLoaded = True
    End Sub

    'PULSANTI IN TABELLA
    Private Sub DgvAnagCommesse_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DgvAnagCommesse.CellContentClick
        Dim senderGrid = DirectCast(sender, DataGridView)

        If TypeOf senderGrid.Columns(e.ColumnIndex) Is DataGridViewButtonColumn AndAlso e.RowIndex >= 0 Then
            Dim row As DataGridViewRow = senderGrid.Rows(e.RowIndex)
            Dim cell As DataGridViewColumn = senderGrid.Columns(e.ColumnIndex)
            Dim id As Integer = row.Cells("id").Value
            Dim codice As String = row.Cells("codice").Value
            Select Case cell.Name
                Case "azioniEdit" 'EDIT
                    Console.WriteLine("EDIT")
                Case "azioniDelete" 'DELETE
                    Dim deleteMsg = "L'eliminazione di una commessa comporta la cancellazione di tutti i particolari ad essa associati." + vbNewLine + vbNewLine + "Eliminare comunque la commessa " + codice + "?"
                    Dim deleteMsgBox = MessageBox.Show(deleteMsg, "Eliminazione Cliente", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                    If deleteMsgBox = DialogResult.Yes Then
                        DeleteRow("commesse", id)
                        Me.DrawTable()
                    End If
                Case Else 'VIEW
                    Console.WriteLine("VIEW")
            End Select
        End If
    End Sub

    'PULSANTE AGGIUNGI COMMESSA
    Private Sub BtnAnagCommesseAdd_Click(sender As Object, e As EventArgs) Handles BtnAnagCommesseAdd.Click
        FormCommesseAdd.Show()
    End Sub
#End Region

    '#######################################################
#Region "FUNZIONI"
    Private Sub OnOpen() Implements IPage.OnOpen
        Me.DrawTable()
    End Sub

    Public Sub DrawTable() Implements ITablePage.DrawTable
        Dim dbRecord As New DbModel("commesse_dettaglio")

        Dim res As List(Of Dictionary(Of String, Object))
        res = dbRecord.GetAll("codice ASC")

        DgvAnagCommesse.Rows.Clear()
        DgvAnagCommesse.Refresh()

        For Each r As Dictionary(Of String, Object) In res

            'CHECK FILTERS
            'filtri vari

            'ADD ROWS
            DgvAnagCommesse.Rows.Add()
            For Each c As KeyValuePair(Of String, Object) In r
                If DgvAnagCommesse.Columns.Contains(c.Key) Then
                    DgvAnagCommesse.Rows(DgvAnagCommesse.Rows.Count - 1).Cells(c.Key).Value = c.Value
                End If
            Next
        Next
    End Sub

    Private Function DeleteRow(table, id) As Boolean
        Dim dbRecord As New DbModel(table)
        Dim cnd As New Dictionary(Of String, Object)
        cnd.Add("id", id)
        Return dbRecord.Delete(cnd)
    End Function
#End Region
End Class
