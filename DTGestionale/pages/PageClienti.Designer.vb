﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PageClienti
    Inherits System.Windows.Forms.UserControl

    'UserControl esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla mediante l'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PnlMainContainer = New System.Windows.Forms.Panel()
        Me.BtnAnagClientiAdd = New System.Windows.Forms.Button()
        Me.DgvAnagClienti = New System.Windows.Forms.DataGridView()
        Me.id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ragione = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.codice = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.path_directory = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.azioniEdit = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.azioniDelete = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.PnlMainContainer.SuspendLayout()
        CType(Me.DgvAnagClienti, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PnlMainContainer
        '
        Me.PnlMainContainer.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PnlMainContainer.Controls.Add(Me.BtnAnagClientiAdd)
        Me.PnlMainContainer.Controls.Add(Me.DgvAnagClienti)
        Me.PnlMainContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PnlMainContainer.Location = New System.Drawing.Point(0, 0)
        Me.PnlMainContainer.Name = "PnlMainContainer"
        Me.PnlMainContainer.Size = New System.Drawing.Size(1024, 768)
        Me.PnlMainContainer.TabIndex = 0
        '
        'BtnAnagClientiAdd
        '
        Me.BtnAnagClientiAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnAnagClientiAdd.BackColor = System.Drawing.Color.DodgerBlue
        Me.BtnAnagClientiAdd.FlatAppearance.BorderSize = 0
        Me.BtnAnagClientiAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnAnagClientiAdd.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnAnagClientiAdd.ForeColor = System.Drawing.Color.Snow
        Me.BtnAnagClientiAdd.Location = New System.Drawing.Point(856, 8)
        Me.BtnAnagClientiAdd.Name = "BtnAnagClientiAdd"
        Me.BtnAnagClientiAdd.Size = New System.Drawing.Size(162, 48)
        Me.BtnAnagClientiAdd.TabIndex = 3
        Me.BtnAnagClientiAdd.Text = "+ NUOVO"
        Me.BtnAnagClientiAdd.UseVisualStyleBackColor = False
        '
        'DgvAnagClienti
        '
        Me.DgvAnagClienti.AllowUserToAddRows = False
        Me.DgvAnagClienti.AllowUserToDeleteRows = False
        Me.DgvAnagClienti.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DgvAnagClienti.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DgvAnagClienti.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.id, Me.ragione, Me.codice, Me.path_directory, Me.azioniEdit, Me.azioniDelete})
        Me.DgvAnagClienti.Location = New System.Drawing.Point(0, 64)
        Me.DgvAnagClienti.Name = "DgvAnagClienti"
        Me.DgvAnagClienti.ReadOnly = True
        Me.DgvAnagClienti.Size = New System.Drawing.Size(1024, 704)
        Me.DgvAnagClienti.TabIndex = 0
        '
        'id
        '
        Me.id.DataPropertyName = "id"
        Me.id.HeaderText = "id"
        Me.id.Name = "id"
        Me.id.ReadOnly = True
        Me.id.Visible = False
        '
        'ragione
        '
        Me.ragione.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ragione.DataPropertyName = "ragione"
        Me.ragione.FillWeight = 30.0!
        Me.ragione.HeaderText = "Ragione Sociale"
        Me.ragione.Name = "ragione"
        Me.ragione.ReadOnly = True
        '
        'codice
        '
        Me.codice.DataPropertyName = "codice"
        Me.codice.HeaderText = "Codice Cliente"
        Me.codice.Name = "codice"
        Me.codice.ReadOnly = True
        '
        'path_directory
        '
        Me.path_directory.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.path_directory.DataPropertyName = "path_directory"
        Me.path_directory.FillWeight = 60.0!
        Me.path_directory.HeaderText = "Percorso Cartella Cliente"
        Me.path_directory.Name = "path_directory"
        Me.path_directory.ReadOnly = True
        '
        'azioniEdit
        '
        Me.azioniEdit.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.azioniEdit.HeaderText = "Modifica"
        Me.azioniEdit.Name = "azioniEdit"
        Me.azioniEdit.ReadOnly = True
        Me.azioniEdit.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.azioniEdit.Text = "M"
        Me.azioniEdit.UseColumnTextForButtonValue = True
        Me.azioniEdit.Width = 60
        '
        'azioniDelete
        '
        Me.azioniDelete.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.azioniDelete.HeaderText = "Elimina"
        Me.azioniDelete.Name = "azioniDelete"
        Me.azioniDelete.ReadOnly = True
        Me.azioniDelete.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.azioniDelete.Text = "E"
        Me.azioniDelete.UseColumnTextForButtonValue = True
        Me.azioniDelete.Width = 60
        '
        'PageClienti
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Snow
        Me.Controls.Add(Me.PnlMainContainer)
        Me.Name = "PageClienti"
        Me.Size = New System.Drawing.Size(1024, 768)
        Me.PnlMainContainer.ResumeLayout(False)
        CType(Me.DgvAnagClienti, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents PnlMainContainer As Panel
    Friend WithEvents DgvAnagClienti As DataGridView
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents BtnAnagClientiAdd As Button
    Friend WithEvents id As DataGridViewTextBoxColumn
    Friend WithEvents ragione As DataGridViewTextBoxColumn
    Friend WithEvents codice As DataGridViewTextBoxColumn
    Friend WithEvents path_directory As DataGridViewTextBoxColumn
    Friend WithEvents azioniEdit As DataGridViewButtonColumn
    Friend WithEvents azioniDelete As DataGridViewButtonColumn
End Class
