﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class PageFornitori
    Inherits System.Windows.Forms.UserControl

    'UserControl esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla mediante l'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.PnlMainContainer = New System.Windows.Forms.Panel()
        Me.BtnAnagFornitoriAdd = New System.Windows.Forms.Button()
        Me.DgvAnagFornitori = New System.Windows.Forms.DataGridView()
        Me.id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ragione = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nome_directory = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.azioniEdit = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.azioniDelete = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.PnlMainContainer.SuspendLayout()
        CType(Me.DgvAnagFornitori, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PnlMainContainer
        '
        Me.PnlMainContainer.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PnlMainContainer.Controls.Add(Me.BtnAnagFornitoriAdd)
        Me.PnlMainContainer.Controls.Add(Me.DgvAnagFornitori)
        Me.PnlMainContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PnlMainContainer.Location = New System.Drawing.Point(0, 0)
        Me.PnlMainContainer.Name = "PnlMainContainer"
        Me.PnlMainContainer.Size = New System.Drawing.Size(1024, 768)
        Me.PnlMainContainer.TabIndex = 0
        '
        'BtnAnagFornitoriAdd
        '
        Me.BtnAnagFornitoriAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnAnagFornitoriAdd.BackColor = System.Drawing.Color.DodgerBlue
        Me.BtnAnagFornitoriAdd.FlatAppearance.BorderSize = 0
        Me.BtnAnagFornitoriAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnAnagFornitoriAdd.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnAnagFornitoriAdd.ForeColor = System.Drawing.Color.Snow
        Me.BtnAnagFornitoriAdd.Location = New System.Drawing.Point(856, 4)
        Me.BtnAnagFornitoriAdd.Name = "BtnAnagFornitoriAdd"
        Me.BtnAnagFornitoriAdd.Size = New System.Drawing.Size(162, 48)
        Me.BtnAnagFornitoriAdd.TabIndex = 5
        Me.BtnAnagFornitoriAdd.Text = "+ NUOVO"
        Me.BtnAnagFornitoriAdd.UseVisualStyleBackColor = False
        '
        'DgvAnagFornitori
        '
        Me.DgvAnagFornitori.AllowUserToAddRows = False
        Me.DgvAnagFornitori.AllowUserToDeleteRows = False
        Me.DgvAnagFornitori.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DgvAnagFornitori.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DgvAnagFornitori.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.id, Me.ragione, Me.nome_directory, Me.azioniEdit, Me.azioniDelete})
        Me.DgvAnagFornitori.Location = New System.Drawing.Point(0, 60)
        Me.DgvAnagFornitori.Name = "DgvAnagFornitori"
        Me.DgvAnagFornitori.ReadOnly = True
        Me.DgvAnagFornitori.Size = New System.Drawing.Size(1024, 704)
        Me.DgvAnagFornitori.TabIndex = 4
        '
        'id
        '
        Me.id.DataPropertyName = "id"
        Me.id.HeaderText = "id"
        Me.id.Name = "id"
        Me.id.ReadOnly = True
        Me.id.Visible = False
        '
        'ragione
        '
        Me.ragione.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ragione.DataPropertyName = "ragione"
        Me.ragione.FillWeight = 30.0!
        Me.ragione.HeaderText = "Ragione Sociale"
        Me.ragione.Name = "ragione"
        Me.ragione.ReadOnly = True
        '
        'nome_directory
        '
        Me.nome_directory.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.nome_directory.DataPropertyName = "nome_directory"
        Me.nome_directory.FillWeight = 30.0!
        Me.nome_directory.HeaderText = "Nome cartella fornitore"
        Me.nome_directory.Name = "nome_directory"
        Me.nome_directory.ReadOnly = True
        '
        'azioniEdit
        '
        Me.azioniEdit.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.azioniEdit.HeaderText = "Modifica"
        Me.azioniEdit.Name = "azioniEdit"
        Me.azioniEdit.ReadOnly = True
        Me.azioniEdit.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.azioniEdit.Text = "M"
        Me.azioniEdit.UseColumnTextForButtonValue = True
        Me.azioniEdit.Width = 60
        '
        'azioniDelete
        '
        Me.azioniDelete.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.azioniDelete.HeaderText = "Elimina"
        Me.azioniDelete.Name = "azioniDelete"
        Me.azioniDelete.ReadOnly = True
        Me.azioniDelete.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.azioniDelete.Text = "E"
        Me.azioniDelete.UseColumnTextForButtonValue = True
        Me.azioniDelete.Width = 60
        '
        'PageFornitori
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Snow
        Me.Controls.Add(Me.PnlMainContainer)
        Me.Name = "PageFornitori"
        Me.Size = New System.Drawing.Size(1024, 768)
        Me.PnlMainContainer.ResumeLayout(False)
        CType(Me.DgvAnagFornitori, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents PnlMainContainer As Panel
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents BtnAnagFornitoriAdd As Button
    Friend WithEvents DgvAnagFornitori As DataGridView
    Friend WithEvents id As DataGridViewTextBoxColumn
    Friend WithEvents ragione As DataGridViewTextBoxColumn
    Friend WithEvents nome_directory As DataGridViewTextBoxColumn
    Friend WithEvents azioniEdit As DataGridViewButtonColumn
    Friend WithEvents azioniDelete As DataGridViewButtonColumn
End Class
