﻿Public Class PageParticolare
    Implements IPage

    Friend particolare As Dictionary(Of String, Object)
    Friend cliente As Dictionary(Of String, Object)

    Friend distintePath As String
    Friend pdfPath As String

    Private partialDettaglio As Control

    '#######################################################
#Region "EVENTI"
    'gestione eventi qui
#End Region

    '#######################################################
#Region "FUNZIONI"
    Private Sub OnOpen() Implements IPage.OnOpen
        'OTTENGO ID DA DATI PAGINA
        Dim id_particolare = PageManager.data("particolare")("id")

        'OTTENGO IL PARTICOLARE IN OGGETTO
        Dim dbRecord = New DbModel("particolari_dettaglio")
        particolare = dbRecord.GetOne(id_particolare)

        'OTTENGO IL CLIENTE
        dbRecord = New DbModel("clienti")
        cliente = dbRecord.GetOne(particolare("id_cliente"))

        'INIZIALIZZO I CAMPI
        InitFields()

        'RIEMPO IL CONTAINER
        ContainerFill()
    End Sub

    'INIZIALIZZAZIONE DEI CAMPI
    Private Sub InitFields()
        LblCliente.Text = particolare("ragione_cliente")
        LblCommessa.Text = particolare("codice_commessa")
        LblCodice.Text = particolare("codice")
        LblDescrizione.Text = particolare("descrizione")
        LblQuantita.Text = particolare("quantita")

        With LblStato
            .Text = particolare("nome_stato").ToString().ToUpper()
            Select Case particolare("id_stato")
                Case 1, 11, 21
                    .ForeColor = Color.DarkOrange
                Case 12, 22
                    .ForeColor = Color.DodgerBlue
                Case 13, 23
                    .ForeColor = Color.LimeGreen
                Case Else
                    .ForeColor = Color.Black
            End Select
        End With
    End Sub

    'RIEMPIMENTO CONTAINER
    Private Sub ContainerFill()
        ContainerClear()

        'ottengo il path delle distinte, se non esistente mostro label errore
        distintePath = PathManager.GetDistintePath(cliente("path_directory"), particolare("codice_commessa"))

        'ottengo il path del PDF, se non esistente mostro label errore
        pdfPath = PathManager.GetPdfPath(distintePath, particolare("codice"))

        'STATI PARTICOLARE
        Select Case particolare("id_stato")

            '###########################################################################
            Case 1 'PENDENTE
                'INSERISCO IL PARTIAL
                partialDettaglio = New PartialPtcCodice1
                partialDettaglio.Show()
                PnlDettagio.Controls.Add(partialDettaglio)

            '###########################################################################
            Case 11 'ATTESA PREVENTIVI LAVORAZIONE

                'INSERISCO IL PARTIAL
                partialDettaglio = New PartialPtcCodice11
                partialDettaglio.Show()
                PnlDettagio.Controls.Add(partialDettaglio)

        End Select
    End Sub

    'CLEAR DEL CONTAINER
    Private Sub ContainerClear()
        PnlDettagio.Controls.Clear()
        partialDettaglio = Nothing
    End Sub
#End Region

End Class
