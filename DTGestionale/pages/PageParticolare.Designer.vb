﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class PageParticolare
    Inherits System.Windows.Forms.UserControl

    'UserControl esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla mediante l'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.PnlMainContainer = New System.Windows.Forms.Panel()
        Me.LblQuantita = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.PnlDettagio = New System.Windows.Forms.Panel()
        Me.LblStato = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.LblDescrizione = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.LblCodice = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.LblCommessa = New System.Windows.Forms.Label()
        Me.LblCliente = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PnlMainContainer.SuspendLayout()
        Me.SuspendLayout()
        '
        'PnlMainContainer
        '
        Me.PnlMainContainer.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PnlMainContainer.Controls.Add(Me.LblQuantita)
        Me.PnlMainContainer.Controls.Add(Me.Label6)
        Me.PnlMainContainer.Controls.Add(Me.PnlDettagio)
        Me.PnlMainContainer.Controls.Add(Me.LblStato)
        Me.PnlMainContainer.Controls.Add(Me.Label3)
        Me.PnlMainContainer.Controls.Add(Me.LblDescrizione)
        Me.PnlMainContainer.Controls.Add(Me.Label5)
        Me.PnlMainContainer.Controls.Add(Me.LblCodice)
        Me.PnlMainContainer.Controls.Add(Me.Label4)
        Me.PnlMainContainer.Controls.Add(Me.LblCommessa)
        Me.PnlMainContainer.Controls.Add(Me.LblCliente)
        Me.PnlMainContainer.Controls.Add(Me.Label2)
        Me.PnlMainContainer.Controls.Add(Me.Label1)
        Me.PnlMainContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PnlMainContainer.Location = New System.Drawing.Point(0, 0)
        Me.PnlMainContainer.Name = "PnlMainContainer"
        Me.PnlMainContainer.Size = New System.Drawing.Size(1024, 768)
        Me.PnlMainContainer.TabIndex = 0
        '
        'LblQuantita
        '
        Me.LblQuantita.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblQuantita.BackColor = System.Drawing.Color.White
        Me.LblQuantita.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblQuantita.Location = New System.Drawing.Point(808, 48)
        Me.LblQuantita.Name = "LblQuantita"
        Me.LblQuantita.Size = New System.Drawing.Size(200, 24)
        Me.LblQuantita.TabIndex = 19
        Me.LblQuantita.Text = "XXXXX"
        Me.LblQuantita.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.BackColor = System.Drawing.Color.DimGray
        Me.Label6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Snow
        Me.Label6.Location = New System.Drawing.Point(712, 48)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(96, 24)
        Me.Label6.TabIndex = 18
        Me.Label6.Text = "Quantità:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'PnlDettagio
        '
        Me.PnlDettagio.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PnlDettagio.BackColor = System.Drawing.Color.Transparent
        Me.PnlDettagio.Location = New System.Drawing.Point(0, 144)
        Me.PnlDettagio.Name = "PnlDettagio"
        Me.PnlDettagio.Size = New System.Drawing.Size(1024, 624)
        Me.PnlDettagio.TabIndex = 13
        '
        'LblStato
        '
        Me.LblStato.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblStato.BackColor = System.Drawing.Color.Transparent
        Me.LblStato.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblStato.Location = New System.Drawing.Point(112, 104)
        Me.LblStato.Name = "LblStato"
        Me.LblStato.Size = New System.Drawing.Size(896, 40)
        Me.LblStato.TabIndex = 12
        Me.LblStato.Text = "XXXXX"
        Me.LblStato.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.DimGray
        Me.Label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Snow
        Me.Label3.Location = New System.Drawing.Point(16, 112)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(96, 24)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "Stato:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblDescrizione
        '
        Me.LblDescrizione.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblDescrizione.BackColor = System.Drawing.Color.White
        Me.LblDescrizione.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDescrizione.Location = New System.Drawing.Point(112, 80)
        Me.LblDescrizione.Name = "LblDescrizione"
        Me.LblDescrizione.Size = New System.Drawing.Size(896, 24)
        Me.LblDescrizione.TabIndex = 10
        Me.LblDescrizione.Text = "XXXXX"
        Me.LblDescrizione.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.DimGray
        Me.Label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Snow
        Me.Label5.Location = New System.Drawing.Point(16, 80)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(96, 24)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Descrizione:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblCodice
        '
        Me.LblCodice.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblCodice.BackColor = System.Drawing.Color.White
        Me.LblCodice.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCodice.Location = New System.Drawing.Point(112, 48)
        Me.LblCodice.Name = "LblCodice"
        Me.LblCodice.Size = New System.Drawing.Size(592, 24)
        Me.LblCodice.TabIndex = 8
        Me.LblCodice.Text = "XXXXX"
        Me.LblCodice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.DimGray
        Me.Label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Snow
        Me.Label4.Location = New System.Drawing.Point(16, 48)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(96, 24)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Codice:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblCommessa
        '
        Me.LblCommessa.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblCommessa.BackColor = System.Drawing.Color.White
        Me.LblCommessa.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCommessa.Location = New System.Drawing.Point(808, 16)
        Me.LblCommessa.Name = "LblCommessa"
        Me.LblCommessa.Size = New System.Drawing.Size(200, 24)
        Me.LblCommessa.TabIndex = 6
        Me.LblCommessa.Text = "XXXXX"
        Me.LblCommessa.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblCliente
        '
        Me.LblCliente.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblCliente.BackColor = System.Drawing.Color.White
        Me.LblCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCliente.Location = New System.Drawing.Point(112, 16)
        Me.LblCliente.Name = "LblCliente"
        Me.LblCliente.Size = New System.Drawing.Size(592, 24)
        Me.LblCliente.TabIndex = 5
        Me.LblCliente.Text = "XXXXX"
        Me.LblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.BackColor = System.Drawing.Color.DimGray
        Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Snow
        Me.Label2.Location = New System.Drawing.Point(712, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(96, 24)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Commessa:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.DimGray
        Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Snow
        Me.Label1.Location = New System.Drawing.Point(16, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(96, 24)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Cliente:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'PageParticolare
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Snow
        Me.Controls.Add(Me.PnlMainContainer)
        Me.Name = "PageParticolare"
        Me.Size = New System.Drawing.Size(1024, 768)
        Me.PnlMainContainer.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents PnlMainContainer As Panel
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents LblDescrizione As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents LblCodice As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents LblCommessa As Label
    Friend WithEvents LblCliente As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents LblStato As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents PnlDettagio As Panel
    Friend WithEvents LblQuantita As Label
    Friend WithEvents Label6 As Label
End Class
