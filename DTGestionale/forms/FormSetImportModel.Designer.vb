﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormSetImportModel
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla mediante l'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LblColCodice = New System.Windows.Forms.Label()
        Me.LblColDescrizione = New System.Windows.Forms.Label()
        Me.LblColQuantita = New System.Windows.Forms.Label()
        Me.NumColCodice = New System.Windows.Forms.NumericUpDown()
        Me.NumColDescrizione = New System.Windows.Forms.NumericUpDown()
        Me.NumColQuantita = New System.Windows.Forms.NumericUpDown()
        CType(Me.NumColCodice, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumColDescrizione, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumColQuantita, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LblColCodice
        '
        Me.LblColCodice.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblColCodice.Location = New System.Drawing.Point(8, 8)
        Me.LblColCodice.Name = "LblColCodice"
        Me.LblColCodice.Size = New System.Drawing.Size(152, 24)
        Me.LblColCodice.TabIndex = 0
        Me.LblColCodice.Text = "Colonna n. parte"
        Me.LblColCodice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblColDescrizione
        '
        Me.LblColDescrizione.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblColDescrizione.Location = New System.Drawing.Point(8, 40)
        Me.LblColDescrizione.Name = "LblColDescrizione"
        Me.LblColDescrizione.Size = New System.Drawing.Size(152, 24)
        Me.LblColDescrizione.TabIndex = 1
        Me.LblColDescrizione.Text = "Colonna descrizione"
        Me.LblColDescrizione.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblColQuantita
        '
        Me.LblColQuantita.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblColQuantita.Location = New System.Drawing.Point(8, 72)
        Me.LblColQuantita.Name = "LblColQuantita"
        Me.LblColQuantita.Size = New System.Drawing.Size(152, 24)
        Me.LblColQuantita.TabIndex = 2
        Me.LblColQuantita.Text = "Colonna quantità"
        Me.LblColQuantita.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'NumColCodice
        '
        Me.NumColCodice.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.NumColCodice.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumColCodice.Location = New System.Drawing.Point(168, 8)
        Me.NumColCodice.Name = "NumColCodice"
        Me.NumColCodice.Size = New System.Drawing.Size(40, 22)
        Me.NumColCodice.TabIndex = 1
        Me.NumColCodice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.NumColCodice.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'NumColDescrizione
        '
        Me.NumColDescrizione.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.NumColDescrizione.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumColDescrizione.Location = New System.Drawing.Point(168, 40)
        Me.NumColDescrizione.Name = "NumColDescrizione"
        Me.NumColDescrizione.Size = New System.Drawing.Size(40, 22)
        Me.NumColDescrizione.TabIndex = 2
        Me.NumColDescrizione.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.NumColDescrizione.Value = New Decimal(New Integer() {2, 0, 0, 0})
        '
        'NumColQuantita
        '
        Me.NumColQuantita.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.NumColQuantita.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumColQuantita.Location = New System.Drawing.Point(168, 72)
        Me.NumColQuantita.Name = "NumColQuantita"
        Me.NumColQuantita.Size = New System.Drawing.Size(40, 22)
        Me.NumColQuantita.TabIndex = 3
        Me.NumColQuantita.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.NumColQuantita.Value = New Decimal(New Integer() {3, 0, 0, 0})
        '
        'SetImportModelForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Snow
        Me.ClientSize = New System.Drawing.Size(216, 97)
        Me.Controls.Add(Me.NumColQuantita)
        Me.Controls.Add(Me.NumColDescrizione)
        Me.Controls.Add(Me.NumColCodice)
        Me.Controls.Add(Me.LblColQuantita)
        Me.Controls.Add(Me.LblColDescrizione)
        Me.Controls.Add(Me.LblColCodice)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "SetImportModelForm"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.Text = "Imposta modello importazione"
        CType(Me.NumColCodice, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumColDescrizione, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumColQuantita, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents LblColCodice As Label
    Friend WithEvents LblColDescrizione As Label
    Friend WithEvents LblColQuantita As Label
    Friend WithEvents NumColCodice As NumericUpDown
    Friend WithEvents NumColDescrizione As NumericUpDown
    Friend WithEvents NumColQuantita As NumericUpDown
End Class
