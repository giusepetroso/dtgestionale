﻿Public Class FormClientiAdd
    '#######################################################
#Region "EVENTI"
    'PULSANTE ANNULLA
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Hide()
    End Sub

    'PULSANTE SALVA
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If Me.Save() Then
            Me.Hide()
            Me.Dispose()

            CType(PAGES("clienti"), ITablePage).DrawTable()
        End If
    End Sub

    'PULSANTE CERCA DIRECTORY
    Private Sub BtnFindDirectory_Click(sender As Object, e As EventArgs) Handles BtnFindDirectory.Click
        Dim result = FolderBrowserDialog.ShowDialog()

        If result = DialogResult.OK And FolderBrowserDialog.SelectedPath IsNot Nothing Then
            in_path_directory.Text = FolderBrowserDialog.SelectedPath
        End If
    End Sub
#End Region

    '#######################################################
#Region "FUNZIONI"
    Private Function Save() As Boolean
        'VALIDAZIONE VALORI
        Dim valMsg = ValidateValues()
        If valMsg IsNot Nothing Then
            Messenger.ShowWarning(valMsg)
            Return False
        End If

        Dim values As New Dictionary(Of String, Object)
        values.Add("ragione", in_ragione.Text)
        values.Add("codice", in_codice.Text)
        values.Add("path_directory", in_path_directory.Text)
        values.Add("note", in_note.Text)

        Dim dbRecord As New DbModel("clienti")

        Dim res = dbRecord.Insert(values)

        If res Then
            Messenger.ShowSuccess(MessengerSuccess.DB_SAVE)
        Else
            Messenger.ShowError(MessengerError.DB_SAVE)
        End If

        Return res
    End Function

    Private Function ValidateValues() As String
        If in_ragione.Text.Trim() = "" Then Return "Il campo ragione sociale è obbligatorio"
        If in_codice.Text.Trim() = "" Then Return "Il campo codice cliente è obbligatorio"
        If in_path_directory.Text.Trim() = "" Then Return "Il percorso directory cliente è obbligatorio"
        Return Nothing
    End Function
#End Region

End Class