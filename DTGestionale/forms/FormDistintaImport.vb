﻿Imports System.Text
Imports System.IO
Imports GemBox.Spreadsheet

Public Class FormDistintaImport
    'DICHIARO LE LISTE DI INPUT
    Dim inParticolari As Dictionary(Of String, InputParticolariRow)

    'DICHIARO LE VARIABILI DEL MODELLO DI IMPORTAZIONE
    Dim numColCodice = FormSetImportModel.NumColCodice.Value
    Dim numColDescrizione = FormSetImportModel.NumColDescrizione.Value
    Dim numColQuantita = FormSetImportModel.NumColQuantita.Value

    '#######################################################
#Region "EVENTI"
    'LOAD
    Private Sub FormDistintaImport_Load(sender As Object, e As EventArgs) Handles Me.Load
        BtnImporta.Enabled = False
    End Sub

    'PULSANTE APRI FILE
    Private Sub BtnOpenFile_Click(sender As Object, e As EventArgs) Handles BtnOpenFile.Click
        OpenFileDialog.ShowDialog()
    End Sub

    'PULSANTE ELABORA FILE
    Private Sub BtnParseFile_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If ParseFile(TbFilePath.Text) Then FillTabs()
    End Sub

    'CONFERMA DA FILE DIALOG
    Private Sub OpenFileDialog_FileOk(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles OpenFileDialog.FileOk
        TbFilePath.Text = OpenFileDialog.FileName
    End Sub

    'PULSANTE IMPOSTA MODELLO IMPORTAZIONE
    Private Sub BtnSetModel_Click(sender As Object, e As EventArgs) Handles BtnSetModel.Click
        FormSetImportModel.ShowDialog()
    End Sub

    'BOTTONE ANNULLA
    Private Sub BtnCancel_Click(sender As Object, e As EventArgs) Handles BtnCancel.Click
        TbFilePath.Text = ""
        Me.ClearTabs()
        Me.Hide()
        Me.Dispose()
    End Sub

    'BOTTONE IMPORTA
    Private Sub BtnImporta_Click(sender As Object, e As EventArgs) Handles BtnImporta.Click
        If Me.Import Then
            Me.Hide()
            Me.Dispose()

            CType(PAGES("home"), ITablePage).DrawTable()
        End If
    End Sub

    'BOTTONE AGGIUNGI CLIENTE
    Private Sub BtnAddCliente_Click(sender As Object, e As EventArgs) Handles BtnAddCliente.Click
        FormClientiAdd.Show()
    End Sub

    'DROPDOWN CLIENTI
    Private Sub CbCliente_DropDown(sender As Object, e As EventArgs) Handles CbCliente.DropDown
        RefreshClientiDropdown()
    End Sub

    'BOTTONE AGGIUNGI COMMESSA
    Private Sub BtnAddCommessa_Click(sender As Object, e As EventArgs) Handles BtnAddCommessa.Click
        FormCommesseAdd.Show()
    End Sub

    'DROPDOWN COMMESSE
    Private Sub CbCommessa_DropDown(sender As Object, e As EventArgs) Handles CbCommessa.DropDown
        RefreshCommesseDropdown()
    End Sub
#End Region

    '#######################################################
#Region "FUNZIONI"
    Private Function ParseFile(filePath As String) As Boolean
        SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY")

        Try
            'ritorno se file non esiste oppure non ha estensione giusta
            If Not File.Exists(filePath) Then Return False

            Dim ext As String = Split(filePath, ".").Last()
            If ext.ToLower() <> "xlsx" And ext.ToLower() <> "xls" And ext.ToLower() <> "csv" Then Return False

            Dim documento = ExcelFile.Load(filePath)

            inParticolari = New Dictionary(Of String, InputParticolariRow)

            For Each foglio In documento.Worksheets
                Dim rowNo = 0
                For Each row In foglio.Rows
                    rowNo += 1
                    If rowNo > 1 Then
                        Dim categoria = ""
                        Dim codice = ""
                        Dim descrizione = ""
                        Dim quantita As Integer
                        Dim cellNo = 0
                        Dim addRow = False
                        For Each cell In row.AllocatedCells
                            cellNo += 1
                            If cell.ValueType <> CellValueType.Null Then
                                Select Case cellNo
                                    Case numColCodice 'CODICE
                                        Dim codiceGrezzo = cell.StringValue

                                        'se codice vuoto salto al prossimo row
                                        If codiceGrezzo.Trim = "" Then
                                            addRow = False
                                            Exit For
                                        End If

                                        Dim prefissoTipo = codiceGrezzo.Substring(0, 2)
                                        If Char.IsNumber(prefissoTipo) Then
                                            codice = codiceGrezzo
                                        Else
                                            codice = codiceGrezzo.Substring(2)
                                            categoria = prefissoTipo
                                        End If
                                        addRow = True

                                        'se tipo = "#" e ci sono altri row salto al prossimo row
                                        If categoria = "#" And rowNo <= foglio.Rows.Count Then
                                            addRow = False
                                            Exit For
                                        End If
                                    Case numColDescrizione 'DESCRIZIONE
                                        descrizione = cell.StringValue
                                    Case numColQuantita 'QUANTITA
                                        quantita = cell.IntValue
                                End Select
                            End If
                        Next

                        If addRow Then
                            Dim categoria_code As String

                            'in base al tipo lo inserisco in una lista diversa
                            Select Case categoria
                                Case "E_"    'commerciali elettrici
                                    categoria_code = "1"
                                Case "M_"    'commerciali meccanici
                                    categoria_code = "2"
                                Case "P_"    'commerciali pneumatici
                                    categoria_code = "3"
                                Case Else   'costruttivi
                                    categoria_code = "101"
                            End Select

                            'CREO UN NUOVO OGGETTO DI TIPO "InputRow"
                            If inParticolari.ContainsKey(codice) Then
                                Dim editedParticolare = inParticolari(codice)
                                editedParticolare.Quantita += quantita
                                inParticolari(codice) = editedParticolare
                            Else
                                inParticolari.Add(codice, New InputParticolariRow(categoria_code, codice, descrizione, quantita))
                            End If

                        End If
                    End If
                Next
            Next
        Catch ex As Exception
#If DEBUG Then
            Messenger.ShowError(ex.Message)
#End If
            Return False
        End Try

        'sommo le voci multiple


        Return True
    End Function

    Private Sub FillTabs()
        Me.ClearTabs()

        Try
            For Each row As KeyValuePair(Of String, InputParticolariRow) In inParticolari
                Dim part = row.Value
                Select Case part.Categoria
                    Case "1"    'commerciali elettrici
                        DgvCommElettrici.Rows.Add(New String() {part.Codice, part.Descrizione, CStr(part.Quantita)})
                    Case "2"    'commerciali meccanici
                        DgvCommMeccanici.Rows.Add(New String() {part.Codice, part.Descrizione, CStr(part.Quantita)})
                    Case "3"    'commerciali pneumatici
                        DgvCommPneumatici.Rows.Add(New String() {part.Codice, part.Descrizione, CStr(part.Quantita)})
                    Case Else   'costruttivi
                        DgvCostruttivi.Rows.Add(New String() {part.Codice, part.Descrizione, CStr(part.Quantita)})
                End Select
            Next

            'dopo aver riempito la tabella è possibile importare
            BtnImporta.Enabled = True
        Catch ex As Exception
#If DEBUG Then
            Messenger.ShowError(ex.Message)
#End If
        End Try
    End Sub

    Private Sub ClearTabs()
        DgvCommElettrici.Rows.Clear()
        DgvCommMeccanici.Rows.Clear()
        DgvCommPneumatici.Rows.Clear()
        DgvCostruttivi.Rows.Clear()
    End Sub

    Private Sub RefreshClientiDropdown()
        Dim dbRecord As New DbModel("clienti")
        Dim comboSource As New Dictionary(Of String, String)()
        comboSource.Add(0, "Seleziona cliente")
        For Each r As Dictionary(Of String, Object) In dbRecord.GetAll()
            comboSource.Add(r("id"), CStr(r("ragione")))
        Next
        CbCliente.DataSource = New BindingSource(comboSource, Nothing)
        CbCliente.DisplayMember = "Value"
        CbCliente.ValueMember = "Key"
    End Sub

    Private Sub RefreshCommesseDropdown()
        Dim dbRecord As New DbModel("commesse")
        Dim cnd As New Dictionary(Of String, Object)
        cnd.Add("id_cliente", CbCliente.SelectedValue)
        Dim comboSource As New Dictionary(Of String, String)()
        comboSource.Add(0, "Seleziona commessa")
        If CInt(CbCliente.SelectedValue) <= 0 Then Return
        For Each r As Dictionary(Of String, Object) In dbRecord.GetWhere(cnd)
            comboSource.Add(r("id"), CStr(r("codice")))
        Next
        CbCommessa.DataSource = New BindingSource(comboSource, Nothing)
        CbCommessa.DisplayMember = "Value"
        CbCommessa.ValueMember = "Key"
    End Sub

    Private Function ValidateValues() As String
        If CInt(CbCliente.SelectedValue) <= 0 Then Return "Selezionare un cliente"
        If CInt(CbCommessa.SelectedValue) <= 0 Then Return "Selezionare una commessa"

        'controllo se non esistano già accoppiate id_commessa-codice uguali
        Dim nokList As New List(Of String)
        Dim dbRecord As New DbModel("particolari")
        Dim cnd As New Dictionary(Of String, Object)
        cnd.Add("id_commessa", CbCommessa.SelectedValue)
        Dim res = dbRecord.GetWhere(cnd)

        For Each r As Dictionary(Of String, Object) In res
            For Each row As KeyValuePair(Of String, InputParticolariRow) In inParticolari
                Dim part = row.Value
                If r("id_commessa") = CbCommessa.SelectedValue And r("codice") = part.Codice Then
                    nokList.Add(part.Codice + ": " + part.Descrizione)
                End If
            Next
        Next

        If nokList.Count > 0 Then
            Dim retString As String
            retString = "Alcuni particolari sono già presenti su database:" + vbNewLine
            For Each nokField As String In nokList
                retString += vbNewLine + " - " + nokField
            Next
            retString += vbNewLine + vbNewLine + "Correggere e reimportare file distinte"
            Return retString
        End If

        Return Nothing
    End Function

    Private Function Import() As Boolean
        'VALIDAZIONE VALORI
        Dim valMsg = ValidateValues()
        If valMsg IsNot Nothing Then
            Messenger.ShowWarning(valMsg)
            Return False
        End If

        Dim dbRecord As New DbModel("particolari")
        Dim transactionOk = True

        dbRecord.StartTransaction()

        For Each row As KeyValuePair(Of String, InputParticolariRow) In inParticolari
            Dim part = row.Value
            Dim values As New Dictionary(Of String, Object)
            values.Add("id_cliente", CbCliente.SelectedValue)
            values.Add("id_commessa", CbCommessa.SelectedValue)
            values.Add("id_categoria", part.Categoria)
            values.Add("id_stato", 1)
            values.Add("codice", part.Codice)
            values.Add("descrizione", part.Descrizione)
            values.Add("quantita", part.Quantita)

            Dim res = dbRecord.Insert(values)
            If Not res Then
                transactionOk = False
                Exit For
            End If
        Next

        If transactionOk Then
            dbRecord.EndTransaction()
            Messenger.ShowSuccess("Importazione effettuata con successo")
        Else
            dbRecord.CancelTransaction()
            Messenger.ShowError(MessengerError.DB_SAVE)
        End If

        Return transactionOk
    End Function
#End Region

End Class
