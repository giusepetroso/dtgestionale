﻿Public Class FormCommesseAdd

    '#######################################################
#Region "EVENTI"
    'BOTTONE ANNULLA
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Hide()
    End Sub

    'BOTTONE SALVA
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If Me.Save() Then
            Me.Hide()
            Me.Dispose()

            CType(PAGES("commesse"), ITablePage).DrawTable()
        End If
    End Sub

    'COMBO BOX CLIENTI DROPDOWN
    Private Sub CbCliente_DropDown(sender As Object, e As EventArgs) Handles in_id_cliente.DropDown
        RefreshClientiDropdown()
    End Sub

    'PULSANTE AGGIUNGI CLIENTE
    Private Sub BtnAddCliente_Click(sender As Object, e As EventArgs) Handles BtnAddCliente.Click
        FormClientiAdd.Show()
    End Sub

#End Region

    '#######################################################
#Region "FUNZIONI"
    Private Function Save() As Boolean
        'VALIDAZIONE VALORI
        Dim valMsg = ValidateValues()
        If valMsg IsNot Nothing Then
            Messenger.ShowWarning(valMsg)
            Return False
        End If

        Dim values As New Dictionary(Of String, Object)
        values.Add("id_cliente", in_id_cliente.SelectedValue)
        values.Add("codice", in_codice.Text)

        Dim dbRecord As New DbModel("commesse")

        Dim res = dbRecord.Insert(values)

        If res Then
            Messenger.ShowSuccess(MessengerSuccess.DB_SAVE)
        Else
            Messenger.ShowError(MessengerError.DB_SAVE)
        End If

        Return res
    End Function

    Private Function ValidateValues() As String
        If CInt(in_id_cliente.SelectedValue) <= 0 Then Return "Nessun cliente selezionato"
        If in_codice.Text.Trim() = "" Then Return "Codice commessa obbligatorio"
        Return Nothing
    End Function

    Private Sub RefreshClientiDropdown()
        Dim dbRecord As New DbModel("clienti")
        Dim comboSource As New Dictionary(Of String, String)()
        comboSource.Add(0, "Seleziona cliente")
        For Each r As Dictionary(Of String, Object) In dbRecord.GetAll()
            comboSource.Add(r("id"), CStr(r("ragione")))
        Next
        in_id_cliente.DataSource = New BindingSource(comboSource, Nothing)
        in_id_cliente.DisplayMember = "Value"
        in_id_cliente.ValueMember = "Key"
    End Sub
#End Region

End Class