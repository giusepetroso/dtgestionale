﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormDistintaImport
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla mediante l'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabCommElettrici = New System.Windows.Forms.TabPage()
        Me.DgvCommElettrici = New System.Windows.Forms.DataGridView()
        Me.codice = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.descrizione = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.quantita = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabCommMeccanici = New System.Windows.Forms.TabPage()
        Me.DgvCommMeccanici = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabCommPneumatici = New System.Windows.Forms.TabPage()
        Me.DgvCommPneumatici = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabCostruttivi = New System.Windows.Forms.TabPage()
        Me.DgvCostruttivi = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OpenFileDialog = New System.Windows.Forms.OpenFileDialog()
        Me.BtnOpenFile = New System.Windows.Forms.Button()
        Me.TbFilePath = New System.Windows.Forms.TextBox()
        Me.BtnSetModel = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.BtnCancel = New System.Windows.Forms.Button()
        Me.BtnImporta = New System.Windows.Forms.Button()
        Me.CbCliente = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.BtnAddCliente = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CbCommessa = New System.Windows.Forms.ComboBox()
        Me.BtnAddCommessa = New System.Windows.Forms.Button()
        Me.TabControl1.SuspendLayout()
        Me.TabCommElettrici.SuspendLayout()
        CType(Me.DgvCommElettrici, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabCommMeccanici.SuspendLayout()
        CType(Me.DgvCommMeccanici, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabCommPneumatici.SuspendLayout()
        CType(Me.DgvCommPneumatici, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabCostruttivi.SuspendLayout()
        CType(Me.DgvCostruttivi, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.TabCostruttivi)
        Me.TabControl1.Controls.Add(Me.TabCommElettrici)
        Me.TabControl1.Controls.Add(Me.TabCommMeccanici)
        Me.TabControl1.Controls.Add(Me.TabCommPneumatici)
        Me.TabControl1.Location = New System.Drawing.Point(0, 120)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(800, 320)
        Me.TabControl1.TabIndex = 0
        '
        'TabCommElettrici
        '
        Me.TabCommElettrici.Controls.Add(Me.DgvCommElettrici)
        Me.TabCommElettrici.Location = New System.Drawing.Point(4, 22)
        Me.TabCommElettrici.Name = "TabCommElettrici"
        Me.TabCommElettrici.Padding = New System.Windows.Forms.Padding(3)
        Me.TabCommElettrici.Size = New System.Drawing.Size(792, 294)
        Me.TabCommElettrici.TabIndex = 0
        Me.TabCommElettrici.Text = "Commerciali elettrici"
        Me.TabCommElettrici.UseVisualStyleBackColor = True
        '
        'DgvCommElettrici
        '
        Me.DgvCommElettrici.AllowUserToAddRows = False
        Me.DgvCommElettrici.AllowUserToDeleteRows = False
        Me.DgvCommElettrici.AllowUserToOrderColumns = True
        Me.DgvCommElettrici.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DgvCommElettrici.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.codice, Me.descrizione, Me.quantita})
        Me.DgvCommElettrici.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DgvCommElettrici.Location = New System.Drawing.Point(3, 3)
        Me.DgvCommElettrici.Name = "DgvCommElettrici"
        Me.DgvCommElettrici.ReadOnly = True
        Me.DgvCommElettrici.Size = New System.Drawing.Size(786, 288)
        Me.DgvCommElettrici.TabIndex = 0
        '
        'codice
        '
        Me.codice.HeaderText = "Codice"
        Me.codice.Name = "codice"
        Me.codice.ReadOnly = True
        '
        'descrizione
        '
        Me.descrizione.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.descrizione.HeaderText = "Descrizione"
        Me.descrizione.Name = "descrizione"
        Me.descrizione.ReadOnly = True
        '
        'quantita
        '
        Me.quantita.HeaderText = "Quantità"
        Me.quantita.Name = "quantita"
        Me.quantita.ReadOnly = True
        '
        'TabCommMeccanici
        '
        Me.TabCommMeccanici.Controls.Add(Me.DgvCommMeccanici)
        Me.TabCommMeccanici.Location = New System.Drawing.Point(4, 22)
        Me.TabCommMeccanici.Name = "TabCommMeccanici"
        Me.TabCommMeccanici.Padding = New System.Windows.Forms.Padding(3)
        Me.TabCommMeccanici.Size = New System.Drawing.Size(792, 294)
        Me.TabCommMeccanici.TabIndex = 1
        Me.TabCommMeccanici.Text = "Commerciali meccanici"
        Me.TabCommMeccanici.UseVisualStyleBackColor = True
        '
        'DgvCommMeccanici
        '
        Me.DgvCommMeccanici.AllowUserToAddRows = False
        Me.DgvCommMeccanici.AllowUserToDeleteRows = False
        Me.DgvCommMeccanici.AllowUserToOrderColumns = True
        Me.DgvCommMeccanici.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DgvCommMeccanici.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3})
        Me.DgvCommMeccanici.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DgvCommMeccanici.Location = New System.Drawing.Point(3, 3)
        Me.DgvCommMeccanici.Name = "DgvCommMeccanici"
        Me.DgvCommMeccanici.ReadOnly = True
        Me.DgvCommMeccanici.Size = New System.Drawing.Size(786, 288)
        Me.DgvCommMeccanici.TabIndex = 1
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Codice"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Descrizione"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Quantità"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'TabCommPneumatici
        '
        Me.TabCommPneumatici.Controls.Add(Me.DgvCommPneumatici)
        Me.TabCommPneumatici.Location = New System.Drawing.Point(4, 22)
        Me.TabCommPneumatici.Name = "TabCommPneumatici"
        Me.TabCommPneumatici.Padding = New System.Windows.Forms.Padding(3)
        Me.TabCommPneumatici.Size = New System.Drawing.Size(792, 294)
        Me.TabCommPneumatici.TabIndex = 3
        Me.TabCommPneumatici.Text = "Commerciali pneumatici"
        Me.TabCommPneumatici.UseVisualStyleBackColor = True
        '
        'DgvCommPneumatici
        '
        Me.DgvCommPneumatici.AllowUserToAddRows = False
        Me.DgvCommPneumatici.AllowUserToDeleteRows = False
        Me.DgvCommPneumatici.AllowUserToOrderColumns = True
        Me.DgvCommPneumatici.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DgvCommPneumatici.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9})
        Me.DgvCommPneumatici.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DgvCommPneumatici.Location = New System.Drawing.Point(3, 3)
        Me.DgvCommPneumatici.Name = "DgvCommPneumatici"
        Me.DgvCommPneumatici.ReadOnly = True
        Me.DgvCommPneumatici.Size = New System.Drawing.Size(786, 288)
        Me.DgvCommPneumatici.TabIndex = 2
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Codice"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn8.HeaderText = "Descrizione"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Quantità"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        '
        'TabCostruttivi
        '
        Me.TabCostruttivi.Controls.Add(Me.DgvCostruttivi)
        Me.TabCostruttivi.Location = New System.Drawing.Point(4, 22)
        Me.TabCostruttivi.Name = "TabCostruttivi"
        Me.TabCostruttivi.Padding = New System.Windows.Forms.Padding(3)
        Me.TabCostruttivi.Size = New System.Drawing.Size(792, 294)
        Me.TabCostruttivi.TabIndex = 2
        Me.TabCostruttivi.Text = "Costruttivi"
        Me.TabCostruttivi.UseVisualStyleBackColor = True
        '
        'DgvCostruttivi
        '
        Me.DgvCostruttivi.AllowUserToAddRows = False
        Me.DgvCostruttivi.AllowUserToDeleteRows = False
        Me.DgvCostruttivi.AllowUserToOrderColumns = True
        Me.DgvCostruttivi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DgvCostruttivi.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6})
        Me.DgvCostruttivi.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DgvCostruttivi.Location = New System.Drawing.Point(3, 3)
        Me.DgvCostruttivi.Name = "DgvCostruttivi"
        Me.DgvCostruttivi.ReadOnly = True
        Me.DgvCostruttivi.Size = New System.Drawing.Size(786, 288)
        Me.DgvCostruttivi.TabIndex = 2
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Codice"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn5.HeaderText = "Descrizione"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Quantità"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        '
        'OpenFileDialog
        '
        Me.OpenFileDialog.Filter = "Tutti i file tabella|*.xlsx;*.xls;*.csv|File XLSX|*.xlsx|File XLS|*.xls|File CSV|" &
    "*.csv"
        '
        'BtnOpenFile
        '
        Me.BtnOpenFile.BackColor = System.Drawing.Color.DodgerBlue
        Me.BtnOpenFile.FlatAppearance.BorderSize = 0
        Me.BtnOpenFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnOpenFile.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnOpenFile.ForeColor = System.Drawing.Color.White
        Me.BtnOpenFile.Location = New System.Drawing.Point(8, 8)
        Me.BtnOpenFile.Name = "BtnOpenFile"
        Me.BtnOpenFile.Size = New System.Drawing.Size(72, 24)
        Me.BtnOpenFile.TabIndex = 1
        Me.BtnOpenFile.Text = "Apri"
        Me.BtnOpenFile.UseVisualStyleBackColor = False
        '
        'TbFilePath
        '
        Me.TbFilePath.Location = New System.Drawing.Point(88, 8)
        Me.TbFilePath.Multiline = True
        Me.TbFilePath.Name = "TbFilePath"
        Me.TbFilePath.Size = New System.Drawing.Size(400, 24)
        Me.TbFilePath.TabIndex = 2
        '
        'BtnSetModel
        '
        Me.BtnSetModel.BackColor = System.Drawing.Color.DodgerBlue
        Me.BtnSetModel.FlatAppearance.BorderSize = 0
        Me.BtnSetModel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnSetModel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnSetModel.ForeColor = System.Drawing.Color.White
        Me.BtnSetModel.Location = New System.Drawing.Point(496, 8)
        Me.BtnSetModel.Name = "BtnSetModel"
        Me.BtnSetModel.Size = New System.Drawing.Size(120, 24)
        Me.BtnSetModel.TabIndex = 3
        Me.BtnSetModel.Text = "Imposta modello"
        Me.BtnSetModel.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.LimeGreen
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(624, 8)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(96, 24)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Elabora file"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'BtnCancel
        '
        Me.BtnCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.BtnCancel.BackColor = System.Drawing.Color.DimGray
        Me.BtnCancel.FlatAppearance.BorderSize = 0
        Me.BtnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnCancel.ForeColor = System.Drawing.Color.Snow
        Me.BtnCancel.Location = New System.Drawing.Point(240, 440)
        Me.BtnCancel.Name = "BtnCancel"
        Me.BtnCancel.Size = New System.Drawing.Size(162, 48)
        Me.BtnCancel.TabIndex = 13
        Me.BtnCancel.Text = "ANNULLA"
        Me.BtnCancel.UseVisualStyleBackColor = False
        '
        'BtnImporta
        '
        Me.BtnImporta.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.BtnImporta.BackColor = System.Drawing.Color.LimeGreen
        Me.BtnImporta.Enabled = False
        Me.BtnImporta.FlatAppearance.BorderSize = 0
        Me.BtnImporta.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnImporta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnImporta.ForeColor = System.Drawing.Color.Snow
        Me.BtnImporta.Location = New System.Drawing.Point(432, 440)
        Me.BtnImporta.Name = "BtnImporta"
        Me.BtnImporta.Size = New System.Drawing.Size(162, 48)
        Me.BtnImporta.TabIndex = 12
        Me.BtnImporta.Text = "IMPORTA"
        Me.BtnImporta.UseVisualStyleBackColor = False
        '
        'CbCliente
        '
        Me.CbCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CbCliente.FormattingEnabled = True
        Me.CbCliente.Location = New System.Drawing.Point(8, 80)
        Me.CbCliente.Name = "CbCliente"
        Me.CbCliente.Size = New System.Drawing.Size(200, 24)
        Me.CbCliente.TabIndex = 14
        Me.CbCliente.Text = "seleziona cliente"
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(12, 46)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(192, 24)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Cliente"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'BtnAddCliente
        '
        Me.BtnAddCliente.BackColor = System.Drawing.Color.DodgerBlue
        Me.BtnAddCliente.FlatAppearance.BorderSize = 0
        Me.BtnAddCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnAddCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnAddCliente.ForeColor = System.Drawing.Color.White
        Me.BtnAddCliente.Location = New System.Drawing.Point(216, 80)
        Me.BtnAddCliente.Name = "BtnAddCliente"
        Me.BtnAddCliente.Size = New System.Drawing.Size(24, 24)
        Me.BtnAddCliente.TabIndex = 16
        Me.BtnAddCliente.Text = "+"
        Me.BtnAddCliente.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(288, 48)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(192, 24)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Commessa"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CbCommessa
        '
        Me.CbCommessa.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CbCommessa.FormattingEnabled = True
        Me.CbCommessa.Location = New System.Drawing.Point(288, 80)
        Me.CbCommessa.Name = "CbCommessa"
        Me.CbCommessa.Size = New System.Drawing.Size(200, 24)
        Me.CbCommessa.TabIndex = 18
        Me.CbCommessa.Text = "seleziona commessa"
        '
        'BtnAddCommessa
        '
        Me.BtnAddCommessa.BackColor = System.Drawing.Color.DodgerBlue
        Me.BtnAddCommessa.FlatAppearance.BorderSize = 0
        Me.BtnAddCommessa.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnAddCommessa.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnAddCommessa.ForeColor = System.Drawing.Color.White
        Me.BtnAddCommessa.Location = New System.Drawing.Point(496, 80)
        Me.BtnAddCommessa.Name = "BtnAddCommessa"
        Me.BtnAddCommessa.Size = New System.Drawing.Size(24, 24)
        Me.BtnAddCommessa.TabIndex = 19
        Me.BtnAddCommessa.Text = "+"
        Me.BtnAddCommessa.UseVisualStyleBackColor = False
        '
        'FormDistintaImport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 497)
        Me.Controls.Add(Me.BtnAddCommessa)
        Me.Controls.Add(Me.CbCommessa)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.BtnAddCliente)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.CbCliente)
        Me.Controls.Add(Me.BtnCancel)
        Me.Controls.Add(Me.BtnImporta)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.BtnSetModel)
        Me.Controls.Add(Me.TbFilePath)
        Me.Controls.Add(Me.BtnOpenFile)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "FormDistintaImport"
        Me.ShowIcon = False
        Me.Text = "Importa distinta"
        Me.TabControl1.ResumeLayout(False)
        Me.TabCommElettrici.ResumeLayout(False)
        CType(Me.DgvCommElettrici, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabCommMeccanici.ResumeLayout(False)
        CType(Me.DgvCommMeccanici, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabCommPneumatici.ResumeLayout(False)
        CType(Me.DgvCommPneumatici, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabCostruttivi.ResumeLayout(False)
        CType(Me.DgvCostruttivi, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabCommElettrici As TabPage
    Friend WithEvents TabCommMeccanici As TabPage
    Friend WithEvents TabCostruttivi As TabPage
    Friend WithEvents OpenFileDialog As OpenFileDialog
    Friend WithEvents BtnOpenFile As Button
    Friend WithEvents TbFilePath As TextBox
    Friend WithEvents BtnSetModel As Button
    Friend WithEvents DgvCommElettrici As DataGridView
    Friend WithEvents codice As DataGridViewTextBoxColumn
    Friend WithEvents descrizione As DataGridViewTextBoxColumn
    Friend WithEvents quantita As DataGridViewTextBoxColumn
    Friend WithEvents DgvCommMeccanici As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents DgvCostruttivi As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As DataGridViewTextBoxColumn
    Friend WithEvents Button1 As Button
    Friend WithEvents TabCommPneumatici As TabPage
    Friend WithEvents DgvCommPneumatici As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn7 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As DataGridViewTextBoxColumn
    Friend WithEvents BtnCancel As Button
    Friend WithEvents BtnImporta As Button
    Friend WithEvents CbCliente As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents BtnAddCliente As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents CbCommessa As ComboBox
    Friend WithEvents BtnAddCommessa As Button
End Class
