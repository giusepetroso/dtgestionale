﻿Public Class FormParticolariFindSelection
    '#######################################################
#Region "EVENTI"
    'PULSANTE ANNULLA
    Private Sub BtnCancel_Click(sender As Object, e As EventArgs)
        Me.Hide()
    End Sub

    'PULSANTI IN TABELLA
    Private Sub DgvParticolariFindSelection_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DgvParticolariFindSelection.CellContentClick
        Dim senderGrid = DirectCast(sender, DataGridView)

        If TypeOf senderGrid.Columns(e.ColumnIndex) Is DataGridViewButtonColumn AndAlso e.RowIndex >= 0 Then
            Dim row As DataGridViewRow = senderGrid.Rows(e.RowIndex)
            Dim cell As DataGridViewColumn = senderGrid.Columns(e.ColumnIndex)
            Dim id As Integer = row.Cells("id").Value

            Dim params As New Dictionary(Of String, Object)
            params.Add("id", id)
            PageManager.OpenPage("particolare", params)

            Me.Hide()
            Me.Dispose()
        End If
    End Sub
#End Region

    '#######################################################
#Region "FUNZIONI"
    Public Sub DrawTable(particolari As List(Of Dictionary(Of String, Object)))
        DgvParticolariFindSelection.Rows.Clear()
        DgvParticolariFindSelection.Refresh()

        For Each r As Dictionary(Of String, Object) In particolari
            'ADD ROWS
            DgvParticolariFindSelection.Rows.Add()
            For Each c As KeyValuePair(Of String, Object) In r
                If DgvParticolariFindSelection.Columns.Contains(c.Key) Then
                    DgvParticolariFindSelection.Rows(DgvParticolariFindSelection.Rows.Count - 1).Cells(c.Key).Value = c.Value
                End If
            Next
        Next
    End Sub
#End Region

End Class