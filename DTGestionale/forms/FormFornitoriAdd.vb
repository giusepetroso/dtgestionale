﻿Public Class FormFornitoriAdd
    Private lavorazioniSelezionate As List(Of KeyValuePair(Of String, String))
    Private trattamentiSelezionati As List(Of KeyValuePair(Of String, String))

    '#######################################################
#Region "EVENTI"
    'LOAD
    Private Sub FormAnagFornitoriAdd_Load(sender As Object, e As EventArgs) Handles Me.Load
        UpdateLavorazioni()
        UpdateTrattamenti()
    End Sub

    'PULSANTE ANNULLA
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Hide()
    End Sub

    'PULSANTE SALVA
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If Me.Save() Then
            Me.Hide()
            Me.Dispose()

            CType(PAGES("fornitori"), ITablePage).DrawTable()
        End If
    End Sub
#End Region

    '#######################################################
#Region "FUNZIONI"
    Private Function Save() As Boolean
        'OTTENGO LE LAVORAZIONI SELEZIONATE
        lavorazioniSelezionate = New List(Of KeyValuePair(Of String, String))
        For i = 0 To (ChlLavorazioni.Items.Count - 1)
            If Not ChlLavorazioni.GetItemChecked(i) Then Continue For

            'ottengo il fornitore
            lavorazioniSelezionate.Add(ChlLavorazioni.Items(i).Tag)
        Next

        'OTTENGO I TRATTAMENTI SELEZIONATI
        trattamentiSelezionati = New List(Of KeyValuePair(Of String, String))
        For i = 0 To (ChlTrattamenti.Items.Count - 1)
            If Not ChlTrattamenti.GetItemChecked(i) Then Continue For

            'ottengo il fornitore
            trattamentiSelezionati.Add(ChlTrattamenti.Items(i).Tag)
        Next

        'VALIDAZIONE VALORI
        Dim valMsg = ValidateValues()
        If valMsg IsNot Nothing Then
            Messenger.ShowWarning(valMsg)
            Return False
        End If

        Dim dbRecord As New DbModel("fornitori")
        Dim transactionOk = True
        dbRecord.StartTransaction()

        'salvo i valori della tabella fornitori
        Dim values As New Dictionary(Of String, Object)
        values.Add("ragione", in_ragione.Text)
        values.Add("nome_directory", in_nome_directory.Text)
        values.Add("note", in_note.Text)

        If Not dbRecord.Insert(values) Then transactionOk = False

        'ottengo l'id del fornitore appena inserito
        Dim id_fornitore As Integer = dbRecord.GetLastId()

        'salvo i valori della tabella lavorazioni
        For Each l As KeyValuePair(Of String, String) In lavorazioniSelezionate
            dbRecord.TableName = "fornitori_tipi_lavorazione"

            values = New Dictionary(Of String, Object)
            values.Add("id_fornitore", id_fornitore)
            values.Add("id_tipo_lavorazione", l.Key)

            If Not dbRecord.Insert(values) Then transactionOk = False
        Next

        'salvo i valori della tabella trattamenti
        For Each t As KeyValuePair(Of String, String) In trattamentiSelezionati
            dbRecord.TableName = "fornitori_tipi_trattamento"

            values = New Dictionary(Of String, Object)
            values.Add("id_fornitore", id_fornitore)
            values.Add("id_tipo_trattamento", t.Key)

            If Not dbRecord.Insert(values) Then transactionOk = False
        Next

        If transactionOk Then
            dbRecord.EndTransaction()
            Messenger.ShowSuccess(MessengerSuccess.DB_SAVE)
        Else
            dbRecord.CancelTransaction()
            Messenger.ShowError(MessengerError.DB_SAVE)
        End If

        Return transactionOk
    End Function

    Private Function ValidateValues() As String
        If in_ragione.Text.Trim() = "" Then Return "Il campo ragione sociale è obbligatorio"
        If in_nome_directory.Text.Trim() = "" Then in_nome_directory.Text = in_ragione.Text

        If lavorazioniSelezionate.Count <= 0 And trattamentiSelezionati.Count <= 0 Then Return "Selezionare almeno una lavorazione o un trattamento"

        Return Nothing
    End Function

    Private Sub UpdateLavorazioni()
        ChlLavorazioni.Items.Clear()

        'aggiungo le checkbox dentro il contenitore 
        For Each l As KeyValuePair(Of String, String) In FormMain.TIPI_LAVORAZIONE
            Dim ctrl As New Control
            ctrl.Tag = l
            ctrl.Text = l.Value
            ChlLavorazioni.Items.Add(ctrl)
            ChlLavorazioni.DisplayMember = "Text"
        Next
    End Sub

    Private Sub UpdateTrattamenti()
        ChlTrattamenti.Items.Clear()

        'aggiungo le checkbox dentro il contenitore 
        For Each t As KeyValuePair(Of String, String) In FormMain.TIPI_TRATTAMENTO
            Dim ctrl As New Control
            ctrl.Tag = t
            ctrl.Text = t.Value
            ChlTrattamenti.Items.Add(ctrl)
            ChlTrattamenti.DisplayMember = "Text"
        Next
    End Sub
#End Region

End Class