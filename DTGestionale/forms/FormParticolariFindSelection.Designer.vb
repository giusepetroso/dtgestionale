﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormParticolariFindSelection
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla mediante l'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.DgvParticolariFindSelection = New System.Windows.Forms.DataGridView()
        Me.id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.id_cliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.id_commessa = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.id_categoria = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.id_stato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dataora_creazione = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ragione_cliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.codice_commessa = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nome_categoria = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nome_stato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.codice = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.descrizione = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.quantita = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.azioniView = New System.Windows.Forms.DataGridViewButtonColumn()
        CType(Me.DgvParticolariFindSelection, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DgvParticolariFindSelection
        '
        Me.DgvParticolariFindSelection.AllowUserToAddRows = False
        Me.DgvParticolariFindSelection.AllowUserToDeleteRows = False
        Me.DgvParticolariFindSelection.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DgvParticolariFindSelection.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.id, Me.id_cliente, Me.id_commessa, Me.id_categoria, Me.id_stato, Me.dataora_creazione, Me.ragione_cliente, Me.codice_commessa, Me.nome_categoria, Me.nome_stato, Me.codice, Me.descrizione, Me.quantita, Me.azioniView})
        Me.DgvParticolariFindSelection.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DgvParticolariFindSelection.Location = New System.Drawing.Point(0, 0)
        Me.DgvParticolariFindSelection.Name = "DgvParticolariFindSelection"
        Me.DgvParticolariFindSelection.ReadOnly = True
        Me.DgvParticolariFindSelection.Size = New System.Drawing.Size(819, 561)
        Me.DgvParticolariFindSelection.TabIndex = 5
        '
        'id
        '
        Me.id.DataPropertyName = "id"
        Me.id.HeaderText = "id"
        Me.id.Name = "id"
        Me.id.ReadOnly = True
        Me.id.Visible = False
        '
        'id_cliente
        '
        Me.id_cliente.DataPropertyName = "id_cliente"
        Me.id_cliente.HeaderText = "id_cliente"
        Me.id_cliente.Name = "id_cliente"
        Me.id_cliente.ReadOnly = True
        Me.id_cliente.Visible = False
        '
        'id_commessa
        '
        Me.id_commessa.DataPropertyName = "id_commessa"
        Me.id_commessa.HeaderText = "id_commessa"
        Me.id_commessa.Name = "id_commessa"
        Me.id_commessa.ReadOnly = True
        Me.id_commessa.Visible = False
        '
        'id_categoria
        '
        Me.id_categoria.DataPropertyName = "id_categoria"
        Me.id_categoria.HeaderText = "id_categoria"
        Me.id_categoria.Name = "id_categoria"
        Me.id_categoria.ReadOnly = True
        Me.id_categoria.Visible = False
        '
        'id_stato
        '
        Me.id_stato.DataPropertyName = "id_stato"
        Me.id_stato.HeaderText = "id_stato"
        Me.id_stato.Name = "id_stato"
        Me.id_stato.ReadOnly = True
        Me.id_stato.Visible = False
        '
        'dataora_creazione
        '
        Me.dataora_creazione.DataPropertyName = "dataora_creazione"
        Me.dataora_creazione.HeaderText = "dataora_creazione"
        Me.dataora_creazione.Name = "dataora_creazione"
        Me.dataora_creazione.ReadOnly = True
        Me.dataora_creazione.Visible = False
        '
        'ragione_cliente
        '
        Me.ragione_cliente.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ragione_cliente.DataPropertyName = "ragione_cliente"
        Me.ragione_cliente.FillWeight = 10.0!
        Me.ragione_cliente.HeaderText = "Cliente"
        Me.ragione_cliente.Name = "ragione_cliente"
        Me.ragione_cliente.ReadOnly = True
        '
        'codice_commessa
        '
        Me.codice_commessa.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.codice_commessa.DataPropertyName = "codice_commessa"
        Me.codice_commessa.FillWeight = 15.0!
        Me.codice_commessa.HeaderText = "Commessa"
        Me.codice_commessa.Name = "codice_commessa"
        Me.codice_commessa.ReadOnly = True
        '
        'nome_categoria
        '
        Me.nome_categoria.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.nome_categoria.DataPropertyName = "nome_categoria"
        Me.nome_categoria.FillWeight = 20.0!
        Me.nome_categoria.HeaderText = "Categoria"
        Me.nome_categoria.Name = "nome_categoria"
        Me.nome_categoria.ReadOnly = True
        '
        'nome_stato
        '
        Me.nome_stato.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.nome_stato.DataPropertyName = "nome_stato"
        Me.nome_stato.FillWeight = 10.0!
        Me.nome_stato.HeaderText = "Stato"
        Me.nome_stato.Name = "nome_stato"
        Me.nome_stato.ReadOnly = True
        '
        'codice
        '
        Me.codice.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.codice.DataPropertyName = "codice"
        Me.codice.FillWeight = 25.0!
        Me.codice.HeaderText = "Codice particolare"
        Me.codice.Name = "codice"
        Me.codice.ReadOnly = True
        '
        'descrizione
        '
        Me.descrizione.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.descrizione.DataPropertyName = "descrizione"
        Me.descrizione.FillWeight = 40.0!
        Me.descrizione.HeaderText = "Descrizione"
        Me.descrizione.Name = "descrizione"
        Me.descrizione.ReadOnly = True
        '
        'quantita
        '
        Me.quantita.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.quantita.DataPropertyName = "quantita"
        Me.quantita.FillWeight = 5.0!
        Me.quantita.HeaderText = "Qta"
        Me.quantita.Name = "quantita"
        Me.quantita.ReadOnly = True
        '
        'azioniView
        '
        Me.azioniView.HeaderText = "Dettaglio"
        Me.azioniView.Name = "azioniView"
        Me.azioniView.ReadOnly = True
        Me.azioniView.Text = "D"
        Me.azioniView.UseColumnTextForButtonValue = True
        Me.azioniView.Width = 55
        '
        'FormParticolariFindSelection
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(819, 561)
        Me.Controls.Add(Me.DgvParticolariFindSelection)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(800, 600)
        Me.Name = "FormParticolariFindSelection"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.Text = "Seleziona particolare da visualizzare"
        CType(Me.DgvParticolariFindSelection, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DgvParticolariFindSelection As DataGridView
    Friend WithEvents id As DataGridViewTextBoxColumn
    Friend WithEvents id_cliente As DataGridViewTextBoxColumn
    Friend WithEvents id_commessa As DataGridViewTextBoxColumn
    Friend WithEvents id_categoria As DataGridViewTextBoxColumn
    Friend WithEvents id_stato As DataGridViewTextBoxColumn
    Friend WithEvents dataora_creazione As DataGridViewTextBoxColumn
    Friend WithEvents ragione_cliente As DataGridViewTextBoxColumn
    Friend WithEvents codice_commessa As DataGridViewTextBoxColumn
    Friend WithEvents nome_categoria As DataGridViewTextBoxColumn
    Friend WithEvents nome_stato As DataGridViewTextBoxColumn
    Friend WithEvents codice As DataGridViewTextBoxColumn
    Friend WithEvents descrizione As DataGridViewTextBoxColumn
    Friend WithEvents quantita As DataGridViewTextBoxColumn
    Friend WithEvents azioniView As DataGridViewButtonColumn
End Class
