﻿Public Class FormParticolariFind
    '#######################################################
#Region "EVENTI"
    'PULSANTE ANNULLA
    Private Sub BtnCancel_Click(sender As Object, e As EventArgs) Handles BtnCancel.Click
        Me.Hide()
    End Sub

    'PULSANTE SALVA
    Private Sub BtnConfirm_Click(sender As Object, e As EventArgs) Handles BtnConfirm.Click
        If Me.Find() Then
            Me.Hide()
            Me.Dispose()
        End If
    End Sub

    'TEXT BOX CODICE CAMBIATO
    Private Sub in_codice_TextChanged(sender As Object, e As EventArgs) Handles in_codice.TextChanged
        If in_codice.Text.Contains(vbNewLine) Then
            in_codice.Text = in_codice.Text.Trim()
            If Me.Find() Then
                Me.Hide()
                Me.Dispose()
            End If
        End If
    End Sub
#End Region

    '#######################################################
#Region "FUNZIONI"
    Private Function Find() As Boolean
        'VALIDAZIONE VALORI
        Dim valMsg = ValidateValues()
        If valMsg IsNot Nothing Then
            Messenger.ShowWarning(valMsg)
            Return False
        End If

        Dim dbRecord As New DbModel("particolari_dettaglio")
        Dim cnd As New Dictionary(Of String, Object)
        cnd.Add("codice", in_codice.Text.Trim.ToUpper)
        Dim res = dbRecord.GetWhere(cnd)

        If res.Count <= 0 Then
            Messenger.ShowWarning(MessengerWarning.NO_DATA)
            Return False
        ElseIf res.Count > 1 Then
            FormParticolariFindSelection.Show()
            FormParticolariFindSelection.DrawTable(res)
            Return True
        End If

        Dim params As New Dictionary(Of String, Object)
        params.Add("id", res(0)("id"))
        PageManager.OpenPage("particolare", params)

        Return True
    End Function

    Private Function ValidateValues() As String
        If in_codice.Text.Trim() = "" Then Return "Inserire un codice particolare"
        Return Nothing
    End Function
#End Region

End Class