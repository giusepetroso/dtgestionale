﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormMain
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla mediante l'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.PnlHeader = New System.Windows.Forms.Panel()
        Me.LblPageTitle = New System.Windows.Forms.Label()
        Me.BtnToggleMenu = New System.Windows.Forms.Button()
        Me.PnlMainContainer = New System.Windows.Forms.Panel()
        Me.PnlHeader.SuspendLayout()
        Me.SuspendLayout()
        '
        'PnlHeader
        '
        Me.PnlHeader.BackColor = System.Drawing.Color.DarkBlue
        Me.PnlHeader.Controls.Add(Me.LblPageTitle)
        Me.PnlHeader.Controls.Add(Me.BtnToggleMenu)
        Me.PnlHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.PnlHeader.Location = New System.Drawing.Point(0, 0)
        Me.PnlHeader.Name = "PnlHeader"
        Me.PnlHeader.Size = New System.Drawing.Size(1008, 40)
        Me.PnlHeader.TabIndex = 0
        '
        'LblPageTitle
        '
        Me.LblPageTitle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblPageTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPageTitle.ForeColor = System.Drawing.Color.White
        Me.LblPageTitle.Location = New System.Drawing.Point(64, 0)
        Me.LblPageTitle.Name = "LblPageTitle"
        Me.LblPageTitle.Size = New System.Drawing.Size(880, 40)
        Me.LblPageTitle.TabIndex = 3
        Me.LblPageTitle.Text = "HOME"
        Me.LblPageTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BtnToggleMenu
        '
        Me.BtnToggleMenu.BackColor = System.Drawing.Color.Transparent
        Me.BtnToggleMenu.FlatAppearance.BorderSize = 0
        Me.BtnToggleMenu.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.BtnToggleMenu.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue
        Me.BtnToggleMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnToggleMenu.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnToggleMenu.ForeColor = System.Drawing.Color.Snow
        Me.BtnToggleMenu.Location = New System.Drawing.Point(0, 0)
        Me.BtnToggleMenu.Name = "BtnToggleMenu"
        Me.BtnToggleMenu.Size = New System.Drawing.Size(48, 40)
        Me.BtnToggleMenu.TabIndex = 2
        Me.BtnToggleMenu.Text = "☰"
        Me.BtnToggleMenu.UseVisualStyleBackColor = False
        '
        'PnlMainContainer
        '
        Me.PnlMainContainer.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PnlMainContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PnlMainContainer.Location = New System.Drawing.Point(0, 40)
        Me.PnlMainContainer.Name = "PnlMainContainer"
        Me.PnlMainContainer.Size = New System.Drawing.Size(1008, 689)
        Me.PnlMainContainer.TabIndex = 1
        '
        'FormMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Snow
        Me.ClientSize = New System.Drawing.Size(1008, 729)
        Me.Controls.Add(Me.PnlMainContainer)
        Me.Controls.Add(Me.PnlHeader)
        Me.MinimumSize = New System.Drawing.Size(800, 600)
        Me.Name = "FormMain"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "DTGestionale"
        Me.PnlHeader.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents PnlHeader As Panel
    Friend WithEvents BtnToggleMenu As Button
    Friend WithEvents PnlMainContainer As Panel
    Friend WithEvents LblPageTitle As Label
End Class
