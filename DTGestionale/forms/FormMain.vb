﻿Imports System.IO

Public Class FormMain
    'MAIN MENU
    Dim mainMenu As New ControlMainMenu

    'GLOBALS
    Public STATI_PARTICOLARE As New Dictionary(Of String, String)
    Public CATEGORIE_PARTICOLARE As New Dictionary(Of String, String)
    Public TIPI_LAVORAZIONE As New Dictionary(Of String, String)
    Public TIPI_TRATTAMENTO As New Dictionary(Of String, String)

    Public TMP_DIR As String

    '#######################################################
#Region "EVENTI"
    'LOAD
    Private Sub MainForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'MASSIMIZZO FINESTRA
        Me.WindowState = FormWindowState.Maximized

        'INIZIALIZZO IL MENU
        mainMenu.Init()

        'INIZIALIZZO LE PAGINE
        PageManager.InitPages(Me.PnlMainContainer)

        'APRO PAGINA HOME
        PageManager.OpenPage("home")

        'IMPOSTO CARTELLA TEMPORANEA
        TMP_DIR = Path.GetTempPath() + "dtgestionale_tmp"

        Try
            If Directory.Exists(TMP_DIR) Then Directory.Delete(TMP_DIR, True)
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        Finally
            Try
                Directory.CreateDirectory(TMP_DIR)
            Catch ex As Exception
                Console.WriteLine(ex.Message)
            End Try
        End Try

        'OTTENGO DATI GLOBALI
        Dim dbRecord As New DbModel("stati_particolare")
        For Each r As Dictionary(Of String, Object) In dbRecord.GetAll()
            STATI_PARTICOLARE.Add(r("id"), r("nome"))
        Next

        dbRecord = New DbModel("categorie_particolare")
        For Each r As Dictionary(Of String, Object) In dbRecord.GetAll()
            CATEGORIE_PARTICOLARE.Add(r("id"), r("nome"))
        Next

        dbRecord = New DbModel("tipi_lavorazione")
        For Each r As Dictionary(Of String, Object) In dbRecord.GetAll()
            TIPI_LAVORAZIONE.Add(r("id"), r("nome"))
        Next

        dbRecord = New DbModel("tipi_trattamento")
        For Each r As Dictionary(Of String, Object) In dbRecord.GetAll()
            TIPI_TRATTAMENTO.Add(r("id"), r("nome"))
        Next
    End Sub

    'DISPOSED
    Private Sub FormMain_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        If Directory.Exists(TMP_DIR) Then
            Try
                Directory.Delete(TMP_DIR, True)
            Catch ex As Exception
                Console.WriteLine(ex.Message)
            End Try
        End If
    End Sub

    'TOGGLE MENU
    Private Sub BtnToggleMenu_Click(sender As Object, e As EventArgs) Handles BtnToggleMenu.Click
        mainMenu.ToggleMenu()
    End Sub
#End Region

End Class