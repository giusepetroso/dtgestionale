﻿Public Module Messenger
    Public Sub ShowError(message As String, Optional description As String = "")
        If description <> "" Then message += ": " + description
        MessageBox.Show(message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error)
    End Sub

    Public Sub ShowError(errorType As MessengerError, Optional description As String = "")
        Dim message As String = "Errore"

        Select Case errorType
            Case MessengerError.DB_SAVE
                message = "Errore nell'effettuare il salvataggio su database"
            Case MessengerError.DB_UPDATE
                message = "Errore nell'effettuare la modifica su database"
            Case MessengerError.DB_DELETE
                message = "Errore nell'effettuare la cancellazione su database"
        End Select

        Messenger.ShowError(message, description)
    End Sub

    Public Sub ShowWarning(message As String, Optional description As String = "")
        If description <> "" Then message += ": " + description
        MessageBox.Show(message, "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    End Sub

    Public Sub ShowWarning(warningType As MessengerWarning, Optional description As String = "")
        Dim message As String = "Attenzione"

        Select Case warningType
            Case MessengerWarning.NO_DATA
                message = "Nessuna corrispondenza trovata"
        End Select

        Messenger.ShowSuccess(message, description)
    End Sub

    Public Sub ShowSuccess(message As String, Optional description As String = "")
        If description <> "" Then message += ": " + description
        MessageBox.Show(message, "Successo", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Public Sub ShowSuccess(successType As MessengerSuccess, Optional description As String = "")
        Dim message As String = "Errore"

        Select Case successType
            Case MessengerSuccess.DB_SAVE
                message = "Salvataggio su database effettuato con successo"
            Case MessengerSuccess.DB_UPDATE
                message = "Modifica su database effettuata con successo"
            Case MessengerSuccess.DB_DELETE
                message = "Cancellazione su database effettuata con successo"
        End Select

        Messenger.ShowSuccess(message, description)
    End Sub
End Module

Public Enum MessengerError
    DB_SAVE
    DB_UPDATE
    DB_DELETE
End Enum

Public Enum MessengerSuccess
    DB_SAVE
    DB_UPDATE
    DB_DELETE
End Enum

Public Enum MessengerWarning
    NO_DATA
End Enum