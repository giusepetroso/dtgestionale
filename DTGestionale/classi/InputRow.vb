﻿Public Class InputParticolariRow
    Public Property Categoria As String
    Public Property Codice As String
    Public Property Descrizione As String
    Public Property Quantita As Integer

    Public Sub New(categoria As String, codice As String, descrizione As String, quantita As String)
        Me.Categoria = categoria
        Me.Codice = codice
        Me.Descrizione = descrizione
        Me.Quantita = quantita
    End Sub
End Class
