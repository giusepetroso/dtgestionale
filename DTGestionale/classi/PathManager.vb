﻿Imports System.IO

Module PathManager
    Public Function GetDistintePath(clientePath As String, codiceCommessa As String) As Object
        If Directory.Exists(clientePath) Then
            For Each d As String In Directory.GetDirectories(clientePath)
                If d.Contains(codiceCommessa) Then
                    Return d + "\Cad Meccanico\Distinte"
                End If
            Next
        End If
        Return Nothing
    End Function

    Public Function GetPdfPath(distintePath As String, codiceParticolare As String) As Object
        Dim fileName As String = distintePath + "\" + codiceParticolare + ".pdf"
        If File.Exists(fileName) Then Return fileName
        Return Nothing
    End Function
End Module
