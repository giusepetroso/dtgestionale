﻿Imports System.Data.SqlClient

Public Module SQLHelper
    Private TRNS_TRANSACTION As SqlTransaction
    Private TRNS_CONNECTION As SqlConnection

    Public Sub StartTransaction(connectionstring As String, transactionName As String)
        Try
            TRNS_CONNECTION = New SqlConnection(connectionstring)
            TRNS_CONNECTION.Open()

            TRNS_TRANSACTION = TRNS_CONNECTION.BeginTransaction(transactionName)
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try
    End Sub

    Public Sub EndTransaction()
        Try
            TRNS_TRANSACTION.Commit()
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try
        TRNS_CONNECTION.Close()
        TRNS_CONNECTION = Nothing
        TRNS_TRANSACTION = Nothing
    End Sub

    Public Sub CancelTransaction()
        Try
            TRNS_TRANSACTION.Rollback()
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try
        TRNS_CONNECTION.Close()
        TRNS_CONNECTION = Nothing
        TRNS_TRANSACTION = Nothing
    End Sub

    Private Function ExecuteNonQueryInTransaction(commandText As String, commandType As CommandType, Optional parameters As SqlParameter() = Nothing) As Int32
        Using cmd As New SqlCommand(commandText, TRNS_CONNECTION)
            cmd.Transaction = TRNS_TRANSACTION

            cmd.CommandType = commandType
            cmd.Parameters.AddRange(parameters)

            Return cmd.ExecuteNonQuery()
        End Using
    End Function

    ' Set the connection, command, And then execute the command with non query.  
    Public Function ExecuteNonQuery(connectionstring As String, commandText As String, commandType As CommandType, Optional parameters As SqlParameter() = Nothing) As Int32
        If TRNS_CONNECTION IsNot Nothing Then Return ExecuteNonQueryInTransaction(commandText, commandType, parameters)

        If parameters Is Nothing Then parameters = New SqlParameter() {}
        Using conn As New SqlConnection(connectionstring)
            Using cmd As New SqlCommand(commandText, conn)
                ' There're three command types: StoredProcedure, Text, TableDirect. The TableDirect   
                ' type Is only for OLE DB. 
                cmd.CommandType = commandType
                cmd.Parameters.AddRange(parameters)

                conn.Open()
                Return cmd.ExecuteNonQuery()
            End Using
        End Using
    End Function

    ' Set the connection, command, And then execute the command And only return one value.  
    Public Function ExecuteScalar(connectionstring As String, commandText As String, commandType As CommandType, Optional parameters As SqlParameter() = Nothing) As Object
        If parameters Is Nothing Then parameters = New SqlParameter() {}

        Using conn As New SqlConnection(connectionstring)
            Using cmd As New SqlCommand(commandText, conn)
                cmd.CommandType = commandType
                cmd.Parameters.AddRange(parameters)

                conn.Open()
                Return cmd.ExecuteScalar()
            End Using
        End Using
    End Function

    ' Set the connection, command, And then execute the command with query And return the reader.  
    Public Function ExecuteReader(connectionstring As String, commandText As String, commandType As CommandType, Optional parameters As SqlParameter() = Nothing) As SqlDataReader
        'If parameters Is Nothing Then parameters = New SqlParameter() {}

        Dim conn As New SqlConnection(connectionstring)

        Using cmd As New SqlCommand(commandText, conn)
            cmd.CommandType = commandType
            If parameters IsNot Nothing Then cmd.Parameters.AddRange(parameters)

            conn.Open()
            ' When using CommandBehavior.CloseConnection, the connection will be closed when the   
            ' IDataReader Is closed.  
            Dim reader As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

            Return reader
        End Using
    End Function

End Module
