﻿Imports System
Public Module PageManager
    Public PAGES As Dictionary(Of String, Control)
    Public data As Dictionary(Of String, Dictionary(Of String, Object))
    Private titles As Dictionary(Of String, String)
    Private activePage As String

    Public Sub InitPages(ByRef context As Control)
        'hardcoding delle pagine
        PAGES = New Dictionary(Of String, Control)
        titles = New Dictionary(Of String, String)
        data = New Dictionary(Of String, Dictionary(Of String, Object))

        PAGES.Add("home", New PageHome())
        titles.Add("home", "LISTA PARTICOLARI")
        data.Add("home", New Dictionary(Of String, Object))

        PAGES.Add("clienti", New PageClienti())
        titles.Add("clienti", "CLIENTI")
        data.Add("clienti", New Dictionary(Of String, Object))

        PAGES.Add("commesse", New PageCommesse())
        titles.Add("commesse", "COMMESSE")
        data.Add("commesse", New Dictionary(Of String, Object))

        PAGES.Add("particolare", New PageParticolare())
        titles.Add("particolare", "DETTAGLIO PARTICOLARE")
        data.Add("particolare", New Dictionary(Of String, Object))

        PAGES.Add("fornitori", New PageFornitori())
        titles.Add("fornitori", "FORNITORI")
        data.Add("fornitori", New Dictionary(Of String, Object))

        For Each p As KeyValuePair(Of String, Control) In PAGES
            p.Value.Dock = DockStyle.Fill
            p.Value.Hide()
            context.Controls.Add(p.Value)
        Next

        activePage = "home"
    End Sub

    Public Sub HidePages()
        For Each p As KeyValuePair(Of String, Control) In PAGES
            p.Value.Hide()
        Next
        FormMain.LblPageTitle.Text = titles("home")
    End Sub

    Public Sub OpenPage(pageName As String, Optional pageData As Dictionary(Of String, Object) = Nothing)
        If PAGES.ContainsKey(pageName) Then
            HidePages()
            Try
                PAGES(pageName).Show()
            Catch ex As Exception
                Console.WriteLine(ex.Message)
                PAGES("home").Show()
            Finally
                If pageData IsNot Nothing Then data(pageName) = pageData
                CType(PAGES(pageName), IPage).OnOpen()
                activePage = pageName
                FormMain.LblPageTitle.Text = titles(pageName)
            End Try
        End If
    End Sub

    Public Sub HidePage(pageName As String)
        If PAGES.ContainsKey(pageName) Then PAGES(pageName).Hide()
    End Sub

    Public Sub RefreshPage()
        PageManager.OpenPage(activePage, data(activePage))
    End Sub
End Module
