﻿Imports System.Data.SqlClient

Public Class DbModel_old
    Private connectionString As String

    Private Shared DB_NAME = "dtgestionale"

    Public Property TableName As String

    Public Sub New()
        Me.connectionString = "Server=localhost\SQLEXPRESS;Database=" + DB_NAME + ";Trusted_Connection=True;"
    End Sub

    Public Sub New(tableName As String)
        Me.New
        Me.TableName = tableName
    End Sub

    Public Sub StartTransaction(Optional transactionName = "tstn")
        SQLHelper.StartTransaction(connectionString, transactionName)
    End Sub

    Public Sub EndTransaction()
        SQLHelper.EndTransaction()
    End Sub

    Public Sub CancelTransaction()
        SQLHelper.CancelTransaction()
    End Sub

    Public Function GetLastId() As Integer
        Dim commandText = "SELECT IDENT_CURRENT('" + TableName + "') AS id;"
        Dim res = Nothing
        Try
            res = SQLHelper.ExecuteScalar(connectionString, commandText, CommandType.Text)
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try

        Return res
    End Function

    Public Function GetCustom(commandText As String) As List(Of Dictionary(Of String, Object))
        Dim res = New List(Of Dictionary(Of String, Object))
        Try
            Using reader As SqlDataReader = SQLHelper.ExecuteReader(connectionString, commandText, CommandType.Text)
                While reader.Read
                    Dim tmpList As New Dictionary(Of String, Object)
                    For i = 0 To reader.FieldCount - 1
                        tmpList.Add(reader.GetName(i), reader.GetValue(i))
                    Next
                    res.Add(tmpList)
                End While
            End Using
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try
        Return res
    End Function

    Public Function GetDataTable(Optional orderBy As String = "id ASC") As DataTable
        Dim commandText = "SELECT * FROM " + TableName + " ORDER BY " + orderBy + ";"
        Dim res As New DataTable()
        Try
            Using sqlDataAdapter As New SqlDataAdapter(commandText, connectionString)
                sqlDataAdapter.Fill(res)
            End Using
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try
        Return res
    End Function

    Public Function GetAll(Optional orderBy As String = "id ASC") As List(Of Dictionary(Of String, Object))
        Dim commandText = "SELECT * FROM " + TableName + " ORDER BY " + orderBy + ";"

        Dim res = New List(Of Dictionary(Of String, Object))
        Try
            Using reader As SqlDataReader = SQLHelper.ExecuteReader(connectionString, commandText, CommandType.Text)
                While reader.Read
                    Dim tmpList As New Dictionary(Of String, Object)
                    For i = 0 To reader.FieldCount - 1
                        tmpList.Add(reader.GetName(i), reader.GetValue(i))
                    Next
                    res.Add(tmpList)
                End While
            End Using
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try
        Return res
    End Function

    Public Function GetOne(id As Integer) As Dictionary(Of String, Object)
        Dim commandText = "SELECT * FROM " + TableName + " WHERE id = @id;"

        Dim res As New Dictionary(Of String, Object)
        Try
            Using reader As SqlDataReader = SQLHelper.ExecuteReader(connectionString, commandText, CommandType.Text, New SqlParameter() {New SqlParameter("@id", CStr(id))})
                If reader.Read Then
                    For i = 0 To reader.FieldCount - 1
                        res.Add(reader.GetName(i), reader.GetValue(i))
                    Next
                End If
            End Using
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try
        Return res
    End Function

    Public Function GetWhere(conditions As Dictionary(Of String, Object), Optional orderBy As String = "id ASC") As List(Of Dictionary(Of String, Object))
        Dim whereString = ""
        Dim sqlParams = New List(Of SqlParameter)
        For Each c As KeyValuePair(Of String, Object) In conditions
            whereString += " AND " + c.Key + " = @" + c.Key
            Dim par = New SqlParameter("@" + c.Key, c.Value)
            sqlParams.Add(par)
        Next

        Dim commandText = "SELECT * FROM " + TableName + " WHERE 1 = 1" + whereString + " ORDER BY " + orderBy + ";"

        Dim res = New List(Of Dictionary(Of String, Object))
        Try
            Using reader As SqlDataReader = SQLHelper.ExecuteReader(connectionString, commandText, CommandType.Text, sqlParams.ToArray)
                While reader.Read
                    Dim tmpList As New Dictionary(Of String, Object)
                    For i = 0 To reader.FieldCount - 1
                        tmpList.Add(reader.GetName(i), reader.GetValue(i))
                    Next
                    res.Add(tmpList)
                End While
            End Using
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try
        Return res
    End Function

    Public Function Insert(values As Dictionary(Of String, Object)) As Boolean
        Dim keysString = ""
        Dim valuesString = ""
        Dim sqlParams = New List(Of SqlParameter)
        For Each v As KeyValuePair(Of String, Object) In values
            keysString += v.Key + ","
            valuesString += "@" + v.Key + ","
            Dim par = New SqlParameter("@" + v.Key, v.Value)
            sqlParams.Add(par)
        Next

        keysString = keysString.Trim().Substring(0, keysString.Length - 1) 'rimuovo ultima virgola
        valuesString = valuesString.Trim().Substring(0, valuesString.Length - 1) 'rimuovo ultima virgola

        Dim commandText = "INSERT INTO " + TableName + " (" + keysString + ") VALUES (" + valuesString + ");"
        Dim res = 0
        Try
            res = SQLHelper.ExecuteNonQuery(connectionString, commandText, CommandType.Text, sqlParams.ToArray)
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try
        Return res = 1
    End Function

    Public Function Update(values As Dictionary(Of String, Object), conditions As Dictionary(Of String, Object)) As Boolean
        Dim updateString = ""
        Dim whereString = ""
        Dim sqlParams = New List(Of SqlParameter)

        If conditions.Count = 0 Then Return False

        For Each v As KeyValuePair(Of String, Object) In values
            updateString += v.Key + " = @" + v.Key + ","
            Dim val As Object
            If v.Value Is Nothing Then
                val = DBNull.Value
            Else
                val = v.Value
            End If
            Dim par = New SqlParameter("@" + v.Key, val)
            sqlParams.Add(par)
        Next

        For Each c As KeyValuePair(Of String, Object) In conditions
            whereString += " AND " + c.Key + " = @@" + c.Key
            Dim par = New SqlParameter("@@" + c.Key, c.Value)
            sqlParams.Add(par)
        Next

        updateString = updateString.Trim().Substring(0, updateString.Length - 1) 'rimuovo ultima virgola

        Dim commandText = "UPDATE " + TableName + " SET " + updateString + " WHERE 1 = 1 " + whereString + ";"
        Dim res = 0
        Try
            res = SQLHelper.ExecuteNonQuery(connectionString, commandText, CommandType.Text, sqlParams.ToArray)
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try
        Return res = 1
    End Function

    Public Function Delete(conditions As Dictionary(Of String, Object)) As Boolean
        Dim whereString = ""
        Dim sqlParams = New List(Of SqlParameter)

        If conditions.Count = 0 Then Return False

        For Each c As KeyValuePair(Of String, Object) In conditions
            whereString += " AND " + c.Key + " = @" + c.Key
            Dim par = New SqlParameter("@" + c.Key, c.Value)
            sqlParams.Add(par)
        Next

        Dim commandText = "DELETE FROM " + TableName + " WHERE 1 = 1 " + whereString + ";"
        Dim res = 0
        Try
            res = SQLHelper.ExecuteNonQuery(connectionString, commandText, CommandType.Text, sqlParams.ToArray)
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try
        Return res = 1
    End Function
End Class
