﻿Imports System
Imports System.Data

Imports MySql.Data
Imports MySql.Data.MySqlClient

Public Class DbModel
    Private connectionString As String

    Private CONNECTION As MySqlConnection = Nothing
    Private COMMAND As MySqlCommand = Nothing
    Private TRANSACTION As MySqlTransaction = Nothing
    Private TRANS_STATUS As Boolean = True

    Private Shared DB_NAME = "dtgestionale"

    Public Property TableName As String

    Public Sub New()
        Me.connectionString = "server=192.168.1.70;user=root;database=" + DB_NAME + ";port=3306;password=dertech69"
    End Sub

    Public Sub New(tableName As String)
        Me.New
        Me.TableName = tableName
    End Sub

    Public Sub StartTransaction()
        CancelTransaction()
        If CONNECTION Is Nothing And TRANSACTION Is Nothing Then
            CONNECTION = New MySqlConnection(connectionString)
            CONNECTION.Open()

            TRANSACTION = CONNECTION.BeginTransaction()
            TRANS_STATUS = True
        End If
    End Sub

    Public Sub EndTransaction()
        If CONNECTION IsNot Nothing And TRANSACTION IsNot Nothing Then
            If (Not TRANS_STATUS) Then
                CancelTransaction()
                Exit Sub
            End If

            TRANSACTION.Commit()
            CONNECTION.Close()

            TRANSACTION = Nothing
            CONNECTION = Nothing
        End If
    End Sub

    Public Sub CancelTransaction()
        If CONNECTION IsNot Nothing And TRANSACTION IsNot Nothing Then
            TRANSACTION.Rollback()
            CONNECTION.Close()

            TRANSACTION = Nothing
            CONNECTION = Nothing
        End If
    End Sub

    Public Function GetLastId() As Integer
        Dim res = Nothing
        Dim query = "SELECT LAST_INSERT_ID();"
        Dim conn = CONNECTION
        If (conn Is Nothing) Then
            conn = New MySqlConnection(connectionString)
            conn.Open()
        End If
        Try
            Dim cmd = New MySqlCommand(query, conn)
            res = cmd.ExecuteScalar()
        Catch ex As Exception
            Console.WriteLine(ex.ToString())
            TRANS_STATUS = False
        End Try
        If (CONNECTION Is Nothing) Then conn.Close()
        Return res
    End Function

    Public Function GetCustom(customQuery As String) As List(Of Dictionary(Of String, Object))
        Dim res = New List(Of Dictionary(Of String, Object))
        Dim query = customQuery
        Dim conn = CONNECTION
        If (conn Is Nothing) Then
            conn = New MySqlConnection(connectionString)
            conn.Open()
        End If
        Try
            Dim cmd = New MySqlCommand(query, conn)
            Using rdr As MySqlDataReader = cmd.ExecuteReader()
                While rdr.Read
                    Dim tmpList As New Dictionary(Of String, Object)
                    For i = 0 To rdr.FieldCount - 1
                        tmpList.Add(rdr.GetName(i), rdr.GetValue(i))
                    Next
                    res.Add(tmpList)
                End While
            End Using
        Catch ex As Exception
            Console.WriteLine(ex.ToString())
            TRANS_STATUS = False
        End Try
        If (CONNECTION Is Nothing) Then conn.Close()
        Return res
    End Function

    Public Function GetAll(Optional orderBy As String = "id ASC") As List(Of Dictionary(Of String, Object))
        Dim res = New List(Of Dictionary(Of String, Object))
        Dim query = "SELECT * FROM " + TableName + " ORDER BY " + orderBy + ";"
        Dim conn = CONNECTION
        If (conn Is Nothing) Then
            conn = New MySqlConnection(connectionString)
            conn.Open()
        End If
        Try
            Dim cmd = New MySqlCommand(query, conn)
            Using rdr As MySqlDataReader = cmd.ExecuteReader()
                While rdr.Read
                    Dim tmpList As New Dictionary(Of String, Object)
                    For i = 0 To rdr.FieldCount - 1
                        tmpList.Add(rdr.GetName(i), rdr.GetValue(i))
                    Next
                    res.Add(tmpList)
                End While
            End Using
        Catch ex As Exception
            Console.WriteLine(ex.ToString())
            TRANS_STATUS = False
        End Try
        If (CONNECTION Is Nothing) Then conn.Close()
        Return res
    End Function

    Public Function GetOne(keyAndValue As KeyValuePair(Of String, Object)) As Dictionary(Of String, Object)
        Dim res = New Dictionary(Of String, Object)
        Dim query = "SELECT * FROM " + TableName + " WHERE " + keyAndValue.Key + " = @" + keyAndValue.Key + " LIMIT 1;"
        Dim conn = CONNECTION
        If (conn Is Nothing) Then
            conn = New MySqlConnection(connectionString)
            conn.Open()
        End If
        Try
            Dim cmd = New MySqlCommand(query, conn)
            cmd.Parameters.AddWithValue("@" + keyAndValue.Key, keyAndValue.Value)

            Using rdr As MySqlDataReader = cmd.ExecuteReader()
                While rdr.Read
                    For i = 0 To rdr.FieldCount - 1
                        res.Add(rdr.GetName(i), rdr.GetValue(i))
                    Next
                End While
            End Using
        Catch ex As Exception
            Console.WriteLine(ex.ToString())
            TRANS_STATUS = False
        End Try
        If (CONNECTION Is Nothing) Then conn.Close()
        Return res
    End Function

    Public Function GetOne(id As Integer) As Dictionary(Of String, Object)
        Return Me.GetOne(New KeyValuePair(Of String, Object)("id", id))
    End Function

    Public Function GetWhere(conditions As Dictionary(Of String, Object), Optional orderBy As String = "id ASC") As List(Of Dictionary(Of String, Object))
        Dim res = New List(Of Dictionary(Of String, Object))
        Dim whereString = ""
        Dim mySqlParams = New List(Of MySqlParameter)
        For Each c As KeyValuePair(Of String, Object) In conditions
            whereString += " AND " + c.Key + " = @" + c.Key
            Dim par = New MySqlParameter("@" + c.Key, c.Value)
            mySqlParams.Add(par)
        Next
        Dim query = "SELECT * FROM " + TableName + " WHERE 1 = 1" + whereString + " ORDER BY " + orderBy + ";"
        Dim conn = CONNECTION
        If (conn Is Nothing) Then
            conn = New MySqlConnection(connectionString)
            conn.Open()
        End If
        Try
            Dim cmd = New MySqlCommand(query, conn)
            cmd.Parameters.AddRange(mySqlParams.ToArray)
            Using rdr As MySqlDataReader = cmd.ExecuteReader()
                While rdr.Read
                    Dim tmpList As New Dictionary(Of String, Object)
                    For i = 0 To rdr.FieldCount - 1
                        tmpList.Add(rdr.GetName(i), rdr.GetValue(i))
                    Next
                    res.Add(tmpList)
                End While
            End Using
        Catch ex As Exception
            Console.WriteLine(ex.ToString())
            TRANS_STATUS = False
        End Try
        If (CONNECTION Is Nothing) Then conn.Close()
        Return res
    End Function

    Public Function Insert(values As Dictionary(Of String, Object)) As Boolean
        Dim res = 0
        Dim keysString = ""
        Dim valuesString = ""
        Dim mySqlParams = New List(Of MySqlParameter)
        For Each v As KeyValuePair(Of String, Object) In values
            keysString += v.Key + ","
            valuesString += "@" + v.Key + ","
            Dim par = New MySqlParameter("@" + v.Key, v.Value)
            mySqlParams.Add(par)
        Next
        keysString = keysString.Trim().Substring(0, keysString.Length - 1) 'rimuovo ultima virgola
        valuesString = valuesString.Trim().Substring(0, valuesString.Length - 1) 'rimuovo ultima virgola
        Dim query = "INSERT INTO " + TableName + " (" + keysString + ") VALUES (" + valuesString + ");"

        Dim conn = CONNECTION
        If (conn Is Nothing) Then
            conn = New MySqlConnection(connectionString)
            conn.Open()
        End If
        Try
            Dim cmd = New MySqlCommand(query, conn)
            cmd.Parameters.AddRange(mySqlParams.ToArray)

            res = cmd.ExecuteNonQuery()
        Catch ex As Exception
            Console.WriteLine(ex.ToString())
            TRANS_STATUS = False
        End Try
        If (CONNECTION Is Nothing) Then conn.Close()
        Return res = 1
    End Function

    Public Function Update(values As Dictionary(Of String, Object), conditions As Dictionary(Of String, Object)) As Boolean
        Dim res = 0
        Dim updateString = ""
        Dim whereString = ""
        Dim mySqlParams = New List(Of MySqlParameter)

        If conditions.Count = 0 Then Return False

        For Each v As KeyValuePair(Of String, Object) In values
            updateString += v.Key + " = @" + v.Key + ","
            Dim val As Object
            If v.Value Is Nothing Then
                val = DBNull.Value
            Else
                val = v.Value
            End If
            Dim par = New MySqlParameter("@" + v.Key, val)
            mySqlParams.Add(par)
        Next

        For Each c As KeyValuePair(Of String, Object) In conditions
            whereString += " AND " + c.Key + " = @2" + c.Key
            Dim par = New MySqlParameter("@2" + c.Key, c.Value)
            mySqlParams.Add(par)
        Next

        updateString = updateString.Trim().Substring(0, updateString.Length - 1) 'rimuovo ultima virgola

        Dim query = "UPDATE " + TableName + " SET " + updateString + " WHERE 1 = 1 " + whereString + ";"
        Dim conn = CONNECTION
        If (conn Is Nothing) Then
            conn = New MySqlConnection(connectionString)
            conn.Open()
        End If
        Try
            Dim cmd = New MySqlCommand(query, conn)
            cmd.Parameters.AddRange(mySqlParams.ToArray)

            res = cmd.ExecuteNonQuery()
        Catch ex As Exception
            Console.WriteLine(ex.ToString())
            TRANS_STATUS = False
        End Try
        If (CONNECTION Is Nothing) Then conn.Close()
        Return Res = 1
    End Function

    Public Function Delete(conditions As Dictionary(Of String, Object)) As Boolean
        Dim res = 0
        Dim whereString = ""
        Dim mySqlParams = New List(Of MySqlParameter)

        If conditions.Count = 0 Then Return False

        For Each c As KeyValuePair(Of String, Object) In conditions
            whereString += " AND " + c.Key + " = @" + c.Key
            Dim par = New MySqlParameter("@" + c.Key, c.Value)
            mySqlParams.Add(par)
        Next

        Dim query = "DELETE FROM " + TableName + " WHERE 1 = 1 " + whereString + ";"
        Dim conn = CONNECTION
        If (conn Is Nothing) Then
            conn = New MySqlConnection(connectionString)
            conn.Open()
        End If
        Try
            Dim cmd = New MySqlCommand(query, conn)
            cmd.Parameters.AddRange(mySqlParams.ToArray)

            res = cmd.ExecuteNonQuery()
        Catch ex As Exception
            Console.WriteLine(ex.ToString())
            TRANS_STATUS = False
        End Try
        If (CONNECTION Is Nothing) Then conn.Close()
        Return res = 1
    End Function
End Class
