﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class PartialPtcCodice11
    Inherits System.Windows.Forms.UserControl

    'UserControl esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla mediante l'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.PnlMainContainer = New System.Windows.Forms.Panel()
        Me.DgvPtcCodice11 = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.id_fornitore = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ragione_fornitore = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.data_consegna = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.costo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.azioniAssign = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.PnlMainContainer.SuspendLayout()
        CType(Me.DgvPtcCodice11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PnlMainContainer
        '
        Me.PnlMainContainer.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PnlMainContainer.Controls.Add(Me.DgvPtcCodice11)
        Me.PnlMainContainer.Controls.Add(Me.Label1)
        Me.PnlMainContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PnlMainContainer.Location = New System.Drawing.Point(0, 0)
        Me.PnlMainContainer.Name = "PnlMainContainer"
        Me.PnlMainContainer.Size = New System.Drawing.Size(1024, 768)
        Me.PnlMainContainer.TabIndex = 0
        '
        'DgvPtcCodice11
        '
        Me.DgvPtcCodice11.AllowUserToAddRows = False
        Me.DgvPtcCodice11.AllowUserToDeleteRows = False
        Me.DgvPtcCodice11.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DgvPtcCodice11.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DgvPtcCodice11.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.id, Me.id_fornitore, Me.ragione_fornitore, Me.data_consegna, Me.costo, Me.azioniAssign})
        Me.DgvPtcCodice11.Location = New System.Drawing.Point(16, 56)
        Me.DgvPtcCodice11.Name = "DgvPtcCodice11"
        Me.DgvPtcCodice11.Size = New System.Drawing.Size(992, 696)
        Me.DgvPtcCodice11.TabIndex = 12
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(16, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(187, 20)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "In attesa di preventivo"
        '
        'id
        '
        Me.id.DataPropertyName = "id"
        Me.id.HeaderText = "id"
        Me.id.Name = "id"
        Me.id.ReadOnly = True
        Me.id.Visible = False
        '
        'id_fornitore
        '
        Me.id_fornitore.DataPropertyName = "id_fornitore"
        Me.id_fornitore.HeaderText = "id_fornitore"
        Me.id_fornitore.Name = "id_fornitore"
        Me.id_fornitore.ReadOnly = True
        Me.id_fornitore.Visible = False
        '
        'ragione_fornitore
        '
        Me.ragione_fornitore.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ragione_fornitore.DataPropertyName = "ragione_fornitore"
        Me.ragione_fornitore.FillWeight = 10.0!
        Me.ragione_fornitore.HeaderText = "Fornitore"
        Me.ragione_fornitore.Name = "ragione_fornitore"
        Me.ragione_fornitore.ReadOnly = True
        '
        'data_consegna
        '
        Me.data_consegna.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.data_consegna.DataPropertyName = "data_consegna"
        Me.data_consegna.FillWeight = 10.0!
        Me.data_consegna.HeaderText = "Consegna stimata"
        Me.data_consegna.Name = "data_consegna"
        '
        'costo
        '
        Me.costo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.costo.DataPropertyName = "costo"
        Me.costo.FillWeight = 15.0!
        Me.costo.HeaderText = "Costo [€]"
        Me.costo.Name = "costo"
        '
        'azioniAssign
        '
        Me.azioniAssign.HeaderText = "Assegna"
        Me.azioniAssign.Name = "azioniAssign"
        Me.azioniAssign.ReadOnly = True
        Me.azioniAssign.Text = "A"
        Me.azioniAssign.UseColumnTextForButtonValue = True
        Me.azioniAssign.Width = 55
        '
        'PartialPtcCodice11
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Snow
        Me.Controls.Add(Me.PnlMainContainer)
        Me.Name = "PartialPtcCodice11"
        Me.Size = New System.Drawing.Size(1024, 768)
        Me.PnlMainContainer.ResumeLayout(False)
        Me.PnlMainContainer.PerformLayout()
        CType(Me.DgvPtcCodice11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents PnlMainContainer As Panel
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents DgvPtcCodice11 As DataGridView
    Friend WithEvents id As DataGridViewTextBoxColumn
    Friend WithEvents id_fornitore As DataGridViewTextBoxColumn
    Friend WithEvents ragione_fornitore As DataGridViewTextBoxColumn
    Friend WithEvents data_consegna As DataGridViewTextBoxColumn
    Friend WithEvents costo As DataGridViewTextBoxColumn
    Friend WithEvents azioniAssign As DataGridViewButtonColumn
End Class
