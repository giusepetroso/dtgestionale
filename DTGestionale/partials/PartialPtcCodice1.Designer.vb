﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class PartialPtcCodice1
    Inherits System.Windows.Forms.UserControl

    'UserControl esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla mediante l'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.PnlMainContainer = New System.Windows.Forms.Panel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.WbPdf = New System.Windows.Forms.WebBrowser()
        Me.BtnConferma = New System.Windows.Forms.Button()
        Me.ChlFornitori = New System.Windows.Forms.CheckedListBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.CbTipoLavorazione = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LblWrongPath = New System.Windows.Forms.Label()
        Me.LblNoPdf = New System.Windows.Forms.Label()
        Me.LblPdfOk = New System.Windows.Forms.Label()
        Me.LblPath = New System.Windows.Forms.Label()
        Me.PnlDettagliContainer = New System.Windows.Forms.Panel()
        Me.PnlMainContainer.SuspendLayout()
        Me.PnlDettagliContainer.SuspendLayout()
        Me.SuspendLayout()
        '
        'PnlMainContainer
        '
        Me.PnlMainContainer.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PnlMainContainer.Controls.Add(Me.PnlDettagliContainer)
        Me.PnlMainContainer.Controls.Add(Me.LblWrongPath)
        Me.PnlMainContainer.Controls.Add(Me.LblNoPdf)
        Me.PnlMainContainer.Controls.Add(Me.LblPdfOk)
        Me.PnlMainContainer.Controls.Add(Me.LblPath)
        Me.PnlMainContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PnlMainContainer.Location = New System.Drawing.Point(0, 0)
        Me.PnlMainContainer.Name = "PnlMainContainer"
        Me.PnlMainContainer.Size = New System.Drawing.Size(1024, 768)
        Me.PnlMainContainer.TabIndex = 0
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(16, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(159, 20)
        Me.Label4.TabIndex = 18
        Me.Label4.Text = "Anteprima disegno"
        '
        'WbPdf
        '
        Me.WbPdf.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.WbPdf.Location = New System.Drawing.Point(16, 56)
        Me.WbPdf.MinimumSize = New System.Drawing.Size(20, 20)
        Me.WbPdf.Name = "WbPdf"
        Me.WbPdf.Size = New System.Drawing.Size(664, 616)
        Me.WbPdf.TabIndex = 17
        '
        'BtnConferma
        '
        Me.BtnConferma.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnConferma.BackColor = System.Drawing.Color.DodgerBlue
        Me.BtnConferma.FlatAppearance.BorderSize = 0
        Me.BtnConferma.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnConferma.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnConferma.ForeColor = System.Drawing.Color.Snow
        Me.BtnConferma.Location = New System.Drawing.Point(696, 608)
        Me.BtnConferma.Name = "BtnConferma"
        Me.BtnConferma.Size = New System.Drawing.Size(312, 64)
        Me.BtnConferma.TabIndex = 16
        Me.BtnConferma.Text = "CONFERMA RICHIESTA"
        Me.BtnConferma.UseVisualStyleBackColor = False
        '
        'ChlFornitori
        '
        Me.ChlFornitori.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ChlFornitori.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ChlFornitori.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChlFornitori.FormattingEnabled = True
        Me.ChlFornitori.Location = New System.Drawing.Point(696, 176)
        Me.ChlFornitori.Name = "ChlFornitori"
        Me.ChlFornitori.Size = New System.Drawing.Size(312, 401)
        Me.ChlFornitori.TabIndex = 15
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(696, 136)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(152, 24)
        Me.Label3.TabIndex = 14
        Me.Label3.Text = "Fornitori"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(696, 48)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(152, 24)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "Tipo di lavorazione"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CbTipoLavorazione
        '
        Me.CbTipoLavorazione.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CbTipoLavorazione.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CbTipoLavorazione.FormattingEnabled = True
        Me.CbTipoLavorazione.Location = New System.Drawing.Point(696, 80)
        Me.CbTipoLavorazione.Name = "CbTipoLavorazione"
        Me.CbTipoLavorazione.Size = New System.Drawing.Size(312, 26)
        Me.CbTipoLavorazione.TabIndex = 12
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(696, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(251, 20)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Prepara richieste di preventivo"
        '
        'LblWrongPath
        '
        Me.LblWrongPath.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblWrongPath.BackColor = System.Drawing.Color.Transparent
        Me.LblWrongPath.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblWrongPath.ForeColor = System.Drawing.Color.Crimson
        Me.LblWrongPath.Location = New System.Drawing.Point(0, 0)
        Me.LblWrongPath.Name = "LblWrongPath"
        Me.LblWrongPath.Size = New System.Drawing.Size(1021, 32)
        Me.LblWrongPath.TabIndex = 20
        Me.LblWrongPath.Text = "Percorso cartella distinte non esistente"
        Me.LblWrongPath.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LblWrongPath.Visible = False
        '
        'LblNoPdf
        '
        Me.LblNoPdf.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblNoPdf.BackColor = System.Drawing.Color.Transparent
        Me.LblNoPdf.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblNoPdf.ForeColor = System.Drawing.Color.Crimson
        Me.LblNoPdf.Location = New System.Drawing.Point(0, 0)
        Me.LblNoPdf.Name = "LblNoPdf"
        Me.LblNoPdf.Size = New System.Drawing.Size(1021, 32)
        Me.LblNoPdf.TabIndex = 19
        Me.LblNoPdf.Text = "Disegno non presente nella directory"
        Me.LblNoPdf.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LblNoPdf.Visible = False
        '
        'LblPdfOk
        '
        Me.LblPdfOk.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblPdfOk.BackColor = System.Drawing.Color.Transparent
        Me.LblPdfOk.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPdfOk.ForeColor = System.Drawing.Color.DodgerBlue
        Me.LblPdfOk.Location = New System.Drawing.Point(0, 0)
        Me.LblPdfOk.Name = "LblPdfOk"
        Me.LblPdfOk.Size = New System.Drawing.Size(1021, 32)
        Me.LblPdfOk.TabIndex = 21
        Me.LblPdfOk.Text = "Disegno presente nella directory"
        Me.LblPdfOk.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LblPdfOk.Visible = False
        '
        'LblPath
        '
        Me.LblPath.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblPath.BackColor = System.Drawing.Color.Transparent
        Me.LblPath.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPath.ForeColor = System.Drawing.Color.Black
        Me.LblPath.Location = New System.Drawing.Point(0, 32)
        Me.LblPath.Name = "LblPath"
        Me.LblPath.Size = New System.Drawing.Size(1021, 48)
        Me.LblPath.TabIndex = 22
        Me.LblPath.Text = "path directory"
        Me.LblPath.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'PnlDettagliContainer
        '
        Me.PnlDettagliContainer.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PnlDettagliContainer.Controls.Add(Me.Label4)
        Me.PnlDettagliContainer.Controls.Add(Me.WbPdf)
        Me.PnlDettagliContainer.Controls.Add(Me.ChlFornitori)
        Me.PnlDettagliContainer.Controls.Add(Me.Label1)
        Me.PnlDettagliContainer.Controls.Add(Me.CbTipoLavorazione)
        Me.PnlDettagliContainer.Controls.Add(Me.BtnConferma)
        Me.PnlDettagliContainer.Controls.Add(Me.Label2)
        Me.PnlDettagliContainer.Controls.Add(Me.Label3)
        Me.PnlDettagliContainer.Location = New System.Drawing.Point(0, 80)
        Me.PnlDettagliContainer.Name = "PnlDettagliContainer"
        Me.PnlDettagliContainer.Size = New System.Drawing.Size(1024, 688)
        Me.PnlDettagliContainer.TabIndex = 23
        Me.PnlDettagliContainer.Visible = False
        '
        'PartialPtcCodice1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Snow
        Me.Controls.Add(Me.PnlMainContainer)
        Me.Name = "PartialPtcCodice1"
        Me.Size = New System.Drawing.Size(1024, 768)
        Me.PnlMainContainer.ResumeLayout(False)
        Me.PnlDettagliContainer.ResumeLayout(False)
        Me.PnlDettagliContainer.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents PnlMainContainer As Panel
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents CbTipoLavorazione As ComboBox
    Friend WithEvents ChlFornitori As CheckedListBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents BtnConferma As Button
    Friend WithEvents WbPdf As WebBrowser
    Friend WithEvents Label4 As Label
    Friend WithEvents LblWrongPath As Label
    Friend WithEvents LblNoPdf As Label
    Friend WithEvents LblPdfOk As Label
    Friend WithEvents LblPath As Label
    Friend WithEvents PnlDettagliContainer As Panel
End Class
