﻿Imports System.IO

Public Class PartialPtcCodice1
    Private cliente As Dictionary(Of String, Object)
    Private particolare As Dictionary(Of String, Object)
    Private distintePath As String
    Private pdfPath As String

    Private fornitoriSelezionati As List(Of Dictionary(Of String, Object))


    '#######################################################
#Region "EVENTI"

    'LOAD
    Private Sub PartialPtcCodice1_Load(sender As Object, e As EventArgs) Handles Me.Load
        'imposto il dock a fill
        Me.Dock = DockStyle.Fill

        'prendo dati da parent
        cliente = CType(PAGES("particolare"), PageParticolare).cliente
        particolare = CType(PAGES("particolare"), PageParticolare).particolare
        distintePath = CType(PAGES("particolare"), PageParticolare).distintePath
        pdfPath = CType(PAGES("particolare"), PageParticolare).pdfPath

        'inizializzo labels
        LblWrongPath.Visible = False
        LblNoPdf.Visible = False
        LblPdfOk.Visible = False
        LblPath.Text = ""

        'verifico il path distinte
        If distintePath Is Nothing Then
            LblWrongPath.Visible = True
            LblPath.Text = "Percorso cliente: '" + cliente("path_directory") + "' | Commessa: '" + particolare("codice_commessa") + "'"
        End If

        If distintePath IsNot Nothing And pdfPath Is Nothing Then
            LblNoPdf.Visible = True
            LblPath.Text = "'" + distintePath + "\" + particolare("codice") + ".pdf'"
        End If

        'se tutto ok mostro label path e pannello dettaglio
        If distintePath IsNot Nothing And pdfPath IsNot Nothing Then
            LblPdfOk.Visible = True
            LblPath.Text = "'" + pdfPath + "'"

            PnlDettagliContainer.Visible = True

            'combo box tipo lavorazione
            Dim comboSource As New Dictionary(Of String, String)()
            comboSource.Add(0, "Seleziona tipo di lavorazione")
            For Each t As KeyValuePair(Of String, String) In FormMain.TIPI_LAVORAZIONE
                comboSource.Add(t.Key, t.Value.ToUpper)
            Next
            CbTipoLavorazione.DataSource = New BindingSource(comboSource, Nothing)
            CbTipoLavorazione.DisplayMember = "Value"
            CbTipoLavorazione.ValueMember = "Key"

            If Not IsDBNull(particolare("id_tipo_lavorazione")) Then CbTipoLavorazione.SelectedIndex = particolare("id_tipo_lavorazione")

            'web browser visualizzazione pdf
            Dim tmpPdf = FormMain.TMP_DIR + "\" + particolare("codice") + ".pdf"
            Try
                File.Copy(pdfPath, tmpPdf, True)
            Catch ex As Exception
                Console.WriteLine(ex.Message)
            Finally
                WbPdf.Navigate(tmpPdf)
            End Try

            'update della lista dei fornitori
            UpdateFornitori(CbTipoLavorazione.SelectedIndex)
        End If
    End Sub

    'COMBO BOX TIPO LAVORAZIONE
    Private Sub CbTipoLavorazione_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CbTipoLavorazione.SelectedIndexChanged
        UpdateFornitori(CbTipoLavorazione.SelectedIndex)
    End Sub

    'BOTTONE CONFERMA
    Private Sub BtnConferma_Click(sender As Object, e As EventArgs) Handles BtnConferma.Click
        ConfermaRichiestePreventivo()
    End Sub
#End Region

    '#######################################################
#Region "FUNZIONI"
    Private Sub UpdateFornitori(idTipoLavorazione As Integer)
        ChlFornitori.Items.Clear()

        Dim dbRecord As New DbModel("fornitori")
        Dim res

        If idTipoLavorazione <= 0 Then
            res = dbRecord.GetAll
        Else
            Dim query As String = "SELECT 
                    fornitori.id AS id,
                    fornitori.ragione AS ragione,
                    fornitori.nome_directory AS nome_directory,
                    tipi_lavorazione.id AS id_tipo_lavorazione, 
                    tipi_lavorazione.nome AS nome_tipo_lavorazione 
                    FROM fornitori
                    INNER JOIN fornitori_tipi_lavorazione ON fornitori.id = fornitori_tipi_lavorazione.id_fornitore
                    INNER JOIN tipi_lavorazione ON fornitori_tipi_lavorazione.id_tipo_lavorazione = tipi_lavorazione.id
                    WHERE id_tipo_lavorazione = " + idTipoLavorazione.ToString
            res = dbRecord.GetCustom(query)
        End If

        'aggiungo le checkbox dentro il contenitore dei fornitori
        For Each r As Dictionary(Of String, Object) In res
            Dim ctrl As New Control
            ctrl.Tag = r
            ctrl.Text = r("ragione")
            ChlFornitori.Items.Add(ctrl)
            ChlFornitori.DisplayMember = "Text"
        Next
    End Sub

    Private Sub ConfermaRichiestePreventivo()
        'OTTENGO I FORNITORI SELEZIONATI
        fornitoriSelezionati = New List(Of Dictionary(Of String, Object))
        For i = 0 To (ChlFornitori.Items.Count - 1)
            If Not ChlFornitori.GetItemChecked(i) Then Continue For

            'ottengo il fornitore
            fornitoriSelezionati.Add(ChlFornitori.Items(i).Tag)
        Next

        'VALIDAZIONE VALORI
        Dim valMsg = ValidateValues()
        If valMsg IsNot Nothing Then
            Messenger.ShowWarning(valMsg)
            Return
        End If

        Dim values As New Dictionary(Of String, Object)

        'PER TUTTI I FORNITORI SELEZIONATI
        Dim dbRecord As New DbModel("richieste_preventivo")
        Dim transactionOk = True
        dbRecord.StartTransaction()

        For Each f As Dictionary(Of String, Object) In fornitoriSelezionati
            'creo un nuovo record nella tabella richieste preventivo
            values = New Dictionary(Of String, Object)
            values.Add("id_particolare", particolare("id"))
            values.Add("id_fornitore", f("id"))

            dbRecord.Insert(values)
        Next

        'modifico stato 
        dbRecord.TableName = "particolari"
        values = New Dictionary(Of String, Object)
        values.Add("id_stato", 11) 'imposto lo stato 11 (attesa preventivi lavorazione)
        Dim cnd As New Dictionary(Of String, Object)
        cnd.Add("id", particolare("id")) 'la where dell'update è l'id del particolare
        If Not dbRecord.Update(values, cnd) Then
            Messenger.ShowError(MessengerError.DB_UPDATE)
        End If

        'messaggio di errore se transaction non ok
        If Not transactionOk Then
            Messenger.ShowError(MessengerError.DB_SAVE)
        End If

        'OPERAZIONI SU DIRECTORIES E PDF
        If transactionOk Then
            For Each f As Dictionary(Of String, Object) In fornitoriSelezionati
                'creo directory
                Dim newDir = Directory.CreateDirectory(distintePath + "\" + f("nome_directory"))

                Try
                    File.Copy(pdfPath, newDir.FullName + "\" + particolare("codice") + ".pdf")
                Catch ex As Exception
                    transactionOk = False
                    Messenger.ShowError(ex.Message)
                End Try
            Next
        End If

        'ELIMINO IL FILE PDF DALLA DIRECTORY DI ORIGINE
        If transactionOk Then
            Try
                File.Delete(pdfPath)
            Catch ex As Exception
                transactionOk = False
                Messenger.ShowError("Errore nella cancellazione del pdf nella directory di origine")
            End Try
        End If

        'CHIUDO LA TRANSACTION
        If transactionOk Then
            dbRecord.EndTransaction()

            Dim descrizione As String = ""
            For i = 0 To (ChlFornitori.Items.Count - 1)
                If Not ChlFornitori.GetItemChecked(i) Then Continue For

                'ottengo il fornitore
                Dim f = ChlFornitori.Items(i).Tag

                descrizione += vbNewLine + " - " + f("ragione")
            Next

            Messenger.ShowSuccess("Il particolare è stato assegnato per richiesta di preventivo", descrizione)

            'ricarico pagina
            PageManager.RefreshPage()
        Else
            dbRecord.CancelTransaction()
        End If
    End Sub

    Private Function ValidateValues() As String
        If fornitoriSelezionati.Count <= 0 Then Return "Nessun fornitore selezionato"
        Return Nothing
    End Function
#End Region
End Class
