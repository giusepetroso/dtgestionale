﻿Imports System.IO

Public Class PartialPtcCodice11
    Private particolare As Dictionary(Of String, Object)
    Private distintePath As String
    Private pdfPath As String

    Private cellValueBackup As Object

    '#######################################################
#Region "EVENTI"
    'LOAD
    Private Sub PartialPtcCodice11_Load(sender As Object, e As EventArgs) Handles Me.Load
        'imposto il dock a fill
        Me.Dock = DockStyle.Fill

        'prendo dati da parent
        particolare = CType(PAGES("particolare"), PageParticolare).particolare
        distintePath = CType(PAGES("particolare"), PageParticolare).distintePath
        pdfPath = CType(PAGES("particolare"), PageParticolare).pdfPath

        DrawTable()
    End Sub

    'MODIFICA TABELLA
    Private Sub DgvPtcCodice11_CellBeginEdit(sender As Object, e As DataGridViewCellCancelEventArgs) Handles DgvPtcCodice11.CellBeginEdit
        Dim senderGrid = DirectCast(sender, DataGridView)

        If e.RowIndex >= 0 Then
            Dim row As DataGridViewRow = senderGrid.Rows(e.RowIndex)
            Dim col As DataGridViewColumn = senderGrid.Columns(e.ColumnIndex)
            cellValueBackup = row.Cells(col.Name).Value
        End If
    End Sub

    Private Sub DgvPtcCodice11_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles DgvPtcCodice11.CellEndEdit
        Dim senderGrid = DirectCast(sender, DataGridView)

        If e.RowIndex >= 0 Then
            Dim row As DataGridViewRow = senderGrid.Rows(e.RowIndex)
            Dim col As DataGridViewColumn = senderGrid.Columns(e.ColumnIndex)
            Dim id As Integer = row.Cells("id").Value
            Dim val = row.Cells(col.Name).Value

            'validazione
            Select Case col.Name
                Case "costo"
                    If Not IsNumeric(val) Then
                        row.Cells(col.Name).Value = cellValueBackup
                        Messenger.ShowError("Il campo costo dev'essere numerico")
                        Return
                    End If
            End Select

            'scrittura su DB
            For Each c As DataGridViewColumn In senderGrid.Columns
                If c.Name = col.Name Then
                    Dim dbRecord As New DbModel("richieste_preventivo")
                    Dim cnd As New Dictionary(Of String, Object)
                    cnd.Add("id", id)
                    Dim values As New Dictionary(Of String, Object)
                    values.Add(col.Name, val)

                    Dim res = dbRecord.Update(values, cnd)

                    If res Then
                        Messenger.ShowSuccess(MessengerSuccess.DB_UPDATE)
                    Else
                        row.Cells(col.Name).Value = cellValueBackup
                        Messenger.ShowError(MessengerError.DB_UPDATE)
                    End If
                End If
            Next
        End If
    End Sub

    'PULSANTI IN TABELLA
    Private Sub DgvPtcCodice11_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DgvPtcCodice11.CellContentClick
        Dim senderGrid = DirectCast(sender, DataGridView)

        If TypeOf senderGrid.Columns(e.ColumnIndex) Is DataGridViewButtonColumn AndAlso e.RowIndex >= 0 Then
            Dim row As DataGridViewRow = senderGrid.Rows(e.RowIndex)
            Dim cell As DataGridViewColumn = senderGrid.Columns(e.ColumnIndex)
            Dim id As Integer = row.Cells("id").Value

            Me.AssegnaLavorazione(id)
        End If
    End Sub
#End Region

    '#######################################################
#Region "FUNZIONI"
    Public Sub DrawTable()
        Dim dbRecord As New DbModel("richieste_preventivo_dettaglio")

        Dim res As List(Of Dictionary(Of String, Object))
        Dim cnd As New Dictionary(Of String, Object)
        cnd.Add("id_particolare", particolare("id"))
        res = dbRecord.GetWhere(cnd, "data_consegna DESC, codice_particolare ASC")

        DgvPtcCodice11.Rows.Clear()
        DgvPtcCodice11.Refresh()

        For Each r As Dictionary(Of String, Object) In res
            'ADD ROWS
            DgvPtcCodice11.Rows.Add()
            For Each c As KeyValuePair(Of String, Object) In r
                If DgvPtcCodice11.Columns.Contains(c.Key) Then
                    DgvPtcCodice11.Rows(DgvPtcCodice11.Rows.Count - 1).Cells(c.Key).Value = c.Value
                End If
            Next
        Next
    End Sub

    Public Function AssegnaLavorazione(idRichiestaPreventivo) As Boolean
        Dim dbRecord As New DbModel("richieste_preventivo_dettaglio")

        Dim res As Dictionary(Of String, Object)
        res = dbRecord.GetOne(idRichiestaPreventivo)

        Dim assegnaMsg = "Assegnare la lavorazione a '" + res("ragione_fornitore") + "'?" +
            vbNewLine + vbNewLine

        If Not IsDBNull(res("data_consegna")) Then assegnaMsg += " Data prevista per la consegna: " + res("data_consegna") + vbNewLine
        If Not IsDBNull(res("costo")) Then assegnaMsg += " Costo lavorazione: " + res("costo") + "€"

        Dim assegnaMsgBox = MessageBox.Show(assegnaMsg, "Assegna lavorazione", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If assegnaMsgBox = DialogResult.Yes Then
            'TODO
        End If
    End Function
#End Region

End Class
